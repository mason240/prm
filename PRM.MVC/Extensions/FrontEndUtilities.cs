﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;


namespace PRM.MVC.Extensions
{
    public static class FrontEndUtilities
    {
        /// <summary>
        /// Determine which navigation link has its class set to "active"
        /// </summary>
        /// <param name="html">method extension for HtmlHelper</param>
        /// <param name="controller">controller to compare</param>
        /// <param name="action">action to compare</param>
        /// <returns>"active" is the current route, all others are empty string</returns>
        public static string SetActive(this HtmlHelper html, string controller, string action)
        {
            var routeData = html.ViewContext.RouteData;
            string routeAction = (string)routeData.Values["action"];
            string routeControl = (string)routeData.Values["controller"];
            bool returnActive = ((controller == routeControl) && (action == routeAction));
            return returnActive ? "active" : String.Empty;
        }

        public static string SetActive(this HtmlHelper html, string controller)
        {
            var routeData = html.ViewContext.RouteData;
            string routeControl = (string)routeData.Values["controller"];
            bool returnActive = ((controller == routeControl));
            return returnActive ? "active" : String.Empty;
        }
    }
}