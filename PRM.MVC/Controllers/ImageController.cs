﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PRM.API.Controllers;
using PRM.API.Controllers.Concretes;
using PRM.API.ViewModels.Album;
using Newtonsoft.Json.Linq;
using PRM.Data.Models;
using PRM.Data.Util;

namespace PRM.MVC.Controllers
{
    public class ImageController : BaseController
    {
        private IAlbumApiController _albumApiController;
        private IImageApiController _imageApiController;
        private ILibraryApiController _libraryApiController;

        public ImageController(IAlbumApiController albumApiController,
                                IImageApiController imageApiController,
                                ILibraryApiController libraryApiController)
        {
            _albumApiController = albumApiController;
            _imageApiController = imageApiController;
            _libraryApiController = libraryApiController;
        }

        public ActionResult AddImage(string AlbumId)
        {
            Guid albumId = ParseRoutingGuid(AlbumId);
            if (albumId == Guid.Empty) return BaseItemNotFound();

            var viewModel = _imageApiController.GetAddImageViewModel(albumId);

            return PartialView("_AddImage", viewModel);
        }

       
        [HttpPost]
        [ActionName("MultiImageUpload")]
        public void MultiImageUpload(AddImageViewModel viewModel)
        {
            Guid userId = GetCurrentUserGuidId();
            Guid albumId = viewModel.AlbumId;
            viewModel.Image = new Image();
            viewModel.Image.Description = "Desc";
            viewModel.Image.UploadedByUser = new User() { Id = userId.ToString() };
            viewModel.Image.Id = Guid.NewGuid();
            var result = _imageApiController.AddImageToAlbum(viewModel);
        }

        [HttpPost]
        public ActionResult AddImage(AddImageViewModel viewModel)
        {
            Guid userId = GetCurrentUserGuidId();
            Guid albumId = viewModel.AlbumId;
            viewModel.Image.UploadedByUser = new User() { Id = userId.ToString() };
            var result = _imageApiController.AddImageToAlbum(viewModel);

            return RedirectToAction("Index", "Album", new { AlbumId = albumId.ToString() });
        }

        public ActionResult RotateImage(string AlbumId, string ImageId, string RotationAngle)
        {
            Guid userId = GetCurrentUserGuidId();
            Guid albumId = ParseRoutingGuid(AlbumId);
            Guid imageId = ParseRoutingGuid(ImageId);
            int rotationAngle = ParseRoutingInt(RotationAngle);

            var result = _imageApiController.RotateImage(albumId, imageId, rotationAngle);

            return RedirectToAction("Index", "Album", new { AlbumId = AlbumId });
        }

        public ActionResult DeleteImageFromAlbum(Guid imageId, Guid albumId)
        {
            Guid userId = GetCurrentUserGuidId();

            _imageApiController.DeleteImageFromAlbum(userId, imageId, albumId);

            return RedirectToAction("Index", "Album", new { AlbumId = albumId });
        }

        public ActionResult AjaxAddImage(string AlbumId)
        {
            Guid albumId = ParseRoutingGuid(AlbumId);
            if (albumId == Guid.Empty) return BaseItemNotFound();

            var viewModel = _imageApiController.GetAddImageViewModel(albumId);

            return PartialView("_AjaxAddImage", viewModel);
        }

        [HttpPost]
        public ActionResult AjaxAddImage(AddImageViewModel viewModel)
        {
            //Call to api controller here, similar to AddImage()

            Guid userId = GetCurrentUserGuidId();
            Guid albumId = viewModel.AlbumId;
            viewModel.Image = new PRM.Data.Models.Image()
            {
                ContentyType = new ContentType() { Id = (int)ContentTypesEnum.IMAGE },
                UploadedByUser = new User() { Id = userId.ToString() },
            };


            var result = _imageApiController.AddImageToAlbum(viewModel);

            try
            {
                return PartialView("~/Views/Image/_Image.cshtml", viewModel.Image/*new PRM.Data.Data.Image()*/);
            }
            catch (Exception e)
            {
                dynamic json = new JObject();
                    json.Status = "Error";
                    json.Error = e;
                    json.ImageId = viewModel.Image.Id;
                
                return json;
            }
        }


    }
}