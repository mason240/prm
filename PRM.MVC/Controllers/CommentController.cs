﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PRM.API.Controllers;
using PRM.API.ViewModels.Comments;
using PRM.Data.Util;

namespace PRM.MVC.Controllers
{
    public class CommentController : BaseController
    {
        private ICommentApiController _commentApiController;

        public CommentController(ICommentApiController commentApiController)
        {
            _commentApiController = commentApiController;
        }

        //public ActionResult CreateComment(string ContentId, string ContentTypeId)
        //{

        //}

        public ActionResult CreateComment(string ContentId, string ContentTypeId)
        {
            var userId = GetCurrentUserGuidId();
            var contentId = ParseRoutingGuid(ContentId);
            var contentTypeId = ParseRoutingInt(ContentTypeId);

            var viewModel = _commentApiController.GetCreateCommentViewModel(userId, contentId, contentTypeId);

            return PartialView("_CreateComment", viewModel);
        }

        [HttpPost]
        public ActionResult CreateComment(CreateCommentViewModel viewModel)
        {
            var userId = GetCurrentUserGuidId();
            viewModel.Comment.User.Id = userId.ToString();
            var comment = _commentApiController.CreateComment(viewModel);

            switch (comment.ContentType.Id)
            {
                case (int)ContentTypesEnum.ALBUM: return RedirectToAction("Index", "Album", new { AlbumId = comment.ContentId.ToString() });
            }

            return RedirectToAction("Index","Home");
        }
    }
}