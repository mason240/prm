﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PRM.API.Controllers;
using PRM.API.Controllers.Concrete;
using PRM.API.ViewModels.Invite;

namespace PRM.MVC.Controllers
{
    public class InviteController : BaseController
    {
        /* 
         * TO-DO
        */
        private IInviteApiController _inviteApiController;

        public InviteController(IInviteApiController inviteApiController)
        {
            _inviteApiController = inviteApiController;
        }

        public ActionResult Index(string inviteId)
        {
            // alpha invite recipient winds up here, do the following:
            // o hand off off to API layer to:
            //   - check if id is valid
            //   - check if id is primary or affiliate id
            //   - create user
            //   - create library
            //   - update Invite database table to associate key with UserId & LibraryId
            //   - create Affiliate user
            //   - send invite email to affiliate
            // o redirect new user to home page
            // 
            Guid inviteGuid = Guid.Parse(inviteId);
            _inviteApiController.Index(inviteGuid);
            return RedirectToRoute("Home/Index");
        }

        // TO-DO admin restricted access
        // GET: Invite/Send
        public ActionResult Send()
        {
            var viewModel = _inviteApiController.GetSendInviteViewModel();
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Send(SendInviteViewModel viewModel)
        {
            // TO-DO
            //   
            
            if (ModelState.IsValid)
            {
                if (_inviteApiController.SendInvite(viewModel))
                {
                    // do something if invitation attempt was successful
                    // congrats what a great situation to be in
                }
                else
                {
                    ModelState.AddModelError("", "Email was not sent successfully!");
                    return View(viewModel);
                }

            }
            else
            {
               return View(viewModel);
            }
            return RedirectToAction("Send");
        }
    }
}