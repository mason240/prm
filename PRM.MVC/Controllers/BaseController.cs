﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;

namespace PRM.MVC.Controllers
{
    public class BaseController : Controller
    {
        public BaseController()
        {
            
        }

        public ActionResult BaseError()
        {
            return RedirectToAction("Index", "Home");
        }

        public ActionResult BaseItemNotFound()
        {
            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Parses a GUID from a routing string. Return Guid.Empty if there is an error.
        /// </summary>
        /// <returns></returns>
        protected Guid ParseRoutingGuid(string s)
        {
            if (s.IsNullOrWhiteSpace()) return Guid.Empty;

            Guid guid;

            if (!Guid.TryParse(s, out guid)) return Guid.Empty;

            return guid;
        }

        /// <summary>
        /// Parses an int from a routing string. 
        /// </summary>
        /// <returns></returns>
        protected int ParseRoutingInt(string s)
        {
            if (s.IsNullOrWhiteSpace()) return -1;

            int i;

            if (!int.TryParse(s, out i)) return -1;

            return i;
        }

        /// <summary>
        /// returns the current user Id
        /// </summary>
        /// <returns></returns>
        protected string GetCurrentUserId()
        {
            //Until the IdentityProvider stuff is finished, we'll just use this id.
            return User.Identity.GetUserId();

            //return User.Identity.GetUserId();
        }

        /// <summary>
        /// returns the current user Id
        /// </summary>
        /// <returns></returns>
        protected Guid GetCurrentUserGuidId()
        {
            return new Guid(GetCurrentUserId());
        }

        /// <summary>
        /// returns the current user Id as string
        /// </summary>
        /// <returns></returns>
        //protected string GetCurrentUserStringId()
        //{
        //    //Until the IdentityProvider stuff is finished, we'll just use this id.
        //    return GetCurrentUserId().ToString();

        //    //return User.Identity.GetUserId();
        //}
    }
}