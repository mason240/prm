﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PRM.API.Controllers;
using PRM.API.Controllers.Concretes;
using PRM.API.ViewModels.Album;

namespace PRM.MVC.Controllers
{
    [Authorize]
    public class AlbumController : BaseController
    {
        private ICommentApiController _commentApiController;
        private IAlbumApiController _albumApiController;
        private ILibraryApiController _libraryApiController;

        public AlbumController(IAlbumApiController albumApiController, ILibraryApiController libraryApiController, ICommentApiController commentApiController)
        {
            _albumApiController = albumApiController;
            _commentApiController = commentApiController;
            _libraryApiController = libraryApiController;
        }

        public ActionResult Index(string AlbumId)
        {
            Guid albumGuid = ParseRoutingGuid(AlbumId);
            if (albumGuid == Guid.Empty) return BaseItemNotFound();

            AlbumIndexViewModel viewModel = _albumApiController.GetAlbumIndexViewModel(albumGuid);

            return View(viewModel);
        }

        public ActionResult AlbumList()
        {
            var viewModel = _albumApiController.GetLibrariesWithAlbumsForUser(GetCurrentUserGuidId()).ToList();

            return View(viewModel);
        }

        public ActionResult Create()
        {
            Guid userId = GetCurrentUserGuidId();
            var viewModel = _albumApiController.GetCreateAlbumViewModel(userId);
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Create(CreateAlbumViewModel viewModel)
        {
            var userId = GetCurrentUserId();

            viewModel.Album.CreatedByUser.Id = userId;
            viewModel.Album.TimespanType.Id = 1;

            var album = _albumApiController.CreateAlbum(viewModel);

            if (album.Id == Guid.Empty) return RedirectToAction("BaseError");

            return RedirectToAction("AlbumList");
        }

        public ActionResult Delete(string AlbumId)
        {
            Guid albumGuid = ParseRoutingGuid(AlbumId);
            var result = _albumApiController.DeleteAlbum(albumGuid);
            return RedirectToAction("AlbumList");
        }

    }
}