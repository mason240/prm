﻿using System;
using System.Configuration;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using PRM.Data.Database;
using PRM.Data.Models;


[assembly: OwinStartupAttribute(typeof(PRM.MVC.Startup))]
namespace PRM.MVC
{ 
    public class Startup
    {
        public static Func<UserManager<User>> UserManagerFactory { get; private set; }

        public void Configuration(IAppBuilder app)
        {
            //ConfigureAuth(app);

            // this is the same as before
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login")
            });

            // configure the user manager
            UserManagerFactory = () =>
            {
                var usermanager = new UserManager<User>(
                    new UserStore<User>(new PrmContext()));

                // allow alphanumeric characters in username
                usermanager.UserValidator = new UserValidator<User>(usermanager)
                {
                    AllowOnlyAlphanumericUserNames = false,
                    RequireUniqueEmail = true
                };


                //Account Login Lockout Settings
                //int lockoutTimeSpan = int.Parse(ConfigurationManager.AppSettings["AccountLockoutTimeSpan"]);
                //usermanager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(lockoutTimeSpan);
                //usermanager.MaxFailedAccessAttemptsBeforeLockout = 3;
                //usermanager.UserLockoutEnabledByDefault = true;

                usermanager.PasswordValidator = new PasswordValidator
                {
                    //RequireNonLetterOrDigit = true,
                    //RequireDigit = true,
                    //RequireLowercase = true,
                    //RequireUppercase = true,

                    //Dev settings
                    //RequiredLength = 4,
                    RequireNonLetterOrDigit = false,
                    RequireDigit = false,
                    RequireLowercase = false,
                    RequireUppercase = false,
                };

                return usermanager;
            };
        }
    }


     //public class EmailService : IIdentityMessageService
     //{
     //    public Task SendAsync(IdentityMessage message)
     //    {
     //        // Plug in your email service here to send an email.
     //        return Task.FromResult(0);
     //    }
     //}

     //public class SmsService : IIdentityMessageService
     //{
     //    public Task SendAsync(IdentityMessage message)
     //    {
     //        // Plug in your sms service here to send a text message.
     //        return Task.FromResult(0);
     //    }
     //}

    public static class IdentityExtensions
    {
        //public static async Task<User> FindByEmailForLoginAsync
        //    (this UserManager<User> userManager, string email, string password)
        //{
        //    var username = usernameOrEmail;
        //    if (usernameOrEmail.Contains("@"))
        //    {
        //        var userForEmail = await userManager.FindByEmailAsync(usernameOrEmail);
        //        if (userForEmail != null)
        //        {
        //            username = userForEmail.UserName;
        //        }
        //    }
        //    return await userManager.FindAsync(username, password);
        //}
    }
}

//[assembly: OwinStartupAttribute(typeof(PRM.MVC.Startup))]
//namespace PRM.MVC
//{
//    public partial class Startup
//    {
//        public void Configuration(IAppBuilder app)
//        {
//            ConfigureAuth(app);
//        }
//    }
//}