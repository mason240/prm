﻿using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using PRM.API.Controllers;
using PRM.API.Controllers.Concrete;
using PRM.API.Controllers.Concretes;
using PRM.Data.Database;
using PRM.Service;
using PRM.Service.User.Concrete;
using PRM.Service.User;

namespace PRM.MVC.App_Start
{
    public class IocConfig
    {
        public static void RegisterDependencies()
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            builder.RegisterType<PrmContext>()
                .As<PrmContext>()
                .InstancePerRequest();

            //PRM.API ApiControllers
            builder.RegisterType<AlbumApiController>()
                .As<IAlbumApiController>()
                .InstancePerRequest();

            builder.RegisterType<CommentApiController>()
                .As<ICommentApiController>()
                .InstancePerRequest();

            builder.RegisterType<ImageApiController>()
                .As<IImageApiController>()
                .InstancePerRequest();

            builder.RegisterType<LibraryApiController>()
                .As<ILibraryApiController>()
                .InstancePerRequest();

            builder.RegisterType<InviteApiController>()
                .As<IInviteApiController>()
                .InstancePerRequest();

            builder.RegisterType<InviteService>()
                .As<IInviteService>()
                .InstancePerRequest();

            RegisteredDependancies.RegisterDependencies(builder);

            IContainer container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}