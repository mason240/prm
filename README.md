### Nuget Pagackes ###

If you have issues with Nuget packages, make sure make sure Nuget Package Restore is enabled by left clicking on the solution and see if "Enable NuGet Package Restore" is an option beneath "Manage NuGet Packages for Solution...." If so, click on it and select yes. http://i.imgur.com/z48WM1j.png

If you are still having issues, close the project, go into the file explorer, delete the packages folder, and repoen the project in Visual Studio. Package restore should then redownload the packages.

