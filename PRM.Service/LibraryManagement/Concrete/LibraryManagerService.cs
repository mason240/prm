﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PRM.Data.Data;
using PRM.Service.Image;
using PRM.Service.Image.Concretes;

namespace PRM.Service.LibraryManagement.Concrete
{
    class LibraryManagerService : ILibraryManagerService
    {
        private prm_dbEntities _db;

        public LibraryManagerService()
        {
            _db = new prm_dbEntities();
        }

        public bool PortAlbumToLibrary(Guid AlbumId, Guid DestinationLibraryId, Guid ActingUser)
        {
            Album original = _db.Albums.Find(AlbumId);
            Guid DuplicateAlbumId = DuplicateAlbum(AlbumId, DestinationLibraryId);

            // TODO: Use GetAlbumImages() once it's moved to PRM.Data.Repository.
            IEnumerable<PRM.Data.Data.Image> images = from img in _db.Images
                         join imgAlbum in _db.AlbumImages on img.Id equals imgAlbum.ImageId
                         where imgAlbum.AlbumId == AlbumId
                         select img;
            // end TODO

            foreach (PRM.Data.Data.Image img in images) {
                // TODO: Use CreateImage() once it's moved to PRM.Data.Repository.
                Guid NewImageId = img.Id;

                _db.Images.Add(img);
                _db.SaveChanges();

                Guid DuplicateImageId = img.Id;
                // end TODO

                // TODO: This is duct tape. Use CreateAlbumImage() once it's moved to PRM.Data.Repository.
                var albumImage = new AlbumImage()
                {
                    AlbumId = DuplicateAlbumId,
                    ImageId = DuplicateImageId
                };

                _db.AlbumImages.Add(albumImage);
                _db.SaveChanges();
                // end TODO

                DuplicateImage(NewImageId, DuplicateImageId);
            }

            return false;
        }

        public Guid DuplicateAlbum(Guid AlbumId, Guid DestinationLibraryId)
        {
            Album original = _db.Albums.Find(AlbumId);
            Album copy = original;

            copy.LibraryId = DestinationLibraryId;
            copy.BranchParentId = AlbumId;

            _db.Albums.Add(copy);
            _db.SaveChanges();

            return copy.Id;
        }

        public bool DuplicateImage(Guid NewImageId, Guid DuplicateImageId)
        {
            ImageStorageService _imageStorageService = new ImageStorageService();
            ImageStreamWrapper imgStreamWrapper = _imageStorageService.GetImageStreamWrapper(NewImageId);
            var result = _imageStorageService.SaveImage(DuplicateImageId, imgStreamWrapper.ImageStream, imgStreamWrapper.ContentType);

            return result;

        }
    }
}
