﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRM.Service.LibraryManagement
{
    public interface ILibraryManagerService
    {
        bool PortAlbumToLibrary(Guid AlbumId, Guid DestinationLibraryId, Guid ActingUser);
        Guid DuplicateAlbum(Guid AlbumId, Guid DestinationLibraryId);
        bool DuplicateImage(Guid NewImageId, Guid DuplicateImageId);
    }
}
