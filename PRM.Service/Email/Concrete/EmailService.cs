﻿using System;
using System.Linq;
using System.Threading.Tasks;
using PRM.Service.Email;
using SendGrid.SmtpApi;
using System.Net;
using System.Configuration;
using System.Net.Configuration;
using System.Net.Mail;

namespace PRM.Service.Concrete.Email
{
    public class EmailService : IEmailService
    {
        public void SendEmail(string recipient, string body, string subject)
        {
            SendEmail(new[] { recipient }, body, subject);
        }

        public void SendEmail(string[] recipients, string body, string subject)
        {
            if(!recipients.Any())
            {
                throw new InvalidOperationException("recipeints cannot be empty!");
            }
            
            var smtp = ConfigurationManager.GetSection("system.net/mailSettings/smtp") as SmtpSection;

            var username = smtp.Network.UserName;
            if (string.IsNullOrWhiteSpace(username))
            {
                throw new NullReferenceException("username is emtpy!",
                    new InvalidOperationException("Check to make sure there is a username in the config file"));
            }

            var password = smtp.Network.Password;
            if(string.IsNullOrWhiteSpace(password))
            {
                throw new NullReferenceException("password is emtpy!",
                    new InvalidOperationException("Check to make sure there is a password in the config file"));
            }

            var message = new SendGrid.SendGridMessage
            {
                From = new MailAddress(smtp.From),
                Subject = subject,
                Html = body
            };
            message.AddTo(recipients);

            // setup transport cridentials
            var credentials = new NetworkCredential(username, password);

            // setup transport for mail message
            var transport = new SendGrid.Web(credentials);

            // send mail message
            transport.Deliver(message);
            
        }

        public void SendEmailAsync(string recipient, string body, string subject)
        {
            Task.Run(() => SendEmail(recipient, body, subject));
        }

        public void SendEmailAsync(string[] recipients, string body, string subject)
        {
            Task.Run(() => SendEmail(recipients, body, subject));
        }
    }
}
