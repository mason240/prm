﻿namespace PRM.Service.Email
{
    public interface IEmailService
    {
        /// <summary>
        /// Sends an Email
        /// </summary>
        /// <param name="recipient">Recipient to send to</param>
        /// <param name="body">body of email</param>
        /// <param name="subject">subject of email</param>
        void SendEmail(string recipient, string body, string subject);

        /// <summary>
        /// Sends an Emil
        /// </summary>
        /// <param name="recipients">list of recipients</param>
        /// <param name="body">body of the email</param>
        /// <param name="subject">subject of the email</param>
        void SendEmail(string[] recipients, string body, string subject);

        /// <summary>
        /// Sends an Email
        /// </summary>
        /// <param name="recipient">Recipient to send to</param>
        /// <param name="body">body of email</param>
        /// <param name="subject">subject of email</param>
        void SendEmailAsync(string recipient, string body, string subject);

        /// <summary>
        /// Sends an Emil
        /// </summary>
        /// <param name="recipients">list of recipients</param>
        /// <param name="body">body of the email</param>
        /// <param name="subject">subject of the email</param>
        void SendEmailAsync(string[] recipients, string body, string subject);
    }
}
