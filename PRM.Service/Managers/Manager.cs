﻿using PRM.Data.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace PRM.Service.Managers
{
    public abstract class Manager<TEntity, TServiceModel, TCreateServiceModel, TUpdateServiceModel, TDeleteServiceModel>
        where TEntity : class
            where TServiceModel : class 
                where TCreateServiceModel : class
                    where TUpdateServiceModel : class
                        where TDeleteServiceModel : class
    {
        protected readonly Repository<TEntity> _repository;

        /// <summary>
        /// Gets an IQueryable of the current entity
        /// </summary>
        protected IQueryable<TEntity> Query
        {
            get
            {
                return _repository.Query;
            }
        }

        /// <summary>
        /// Gets the entire entity collection
        /// </summary>
        protected IEnumerable<TServiceModel> All
        {
            get
            {
                return GetAll();
            }
        }

        /// <summary>
        /// Creates a new manager with a base repository
        /// </summary>
        /// <param name="repository"></param>
        public Manager(Repository<TEntity> repository)
        {
            _repository = repository;
        }

        public abstract TServiceModel Create(TCreateServiceModel model);

        public abstract TServiceModel Update(TUpdateServiceModel model);

        public abstract TServiceModel Delete(TDeleteServiceModel model);

        protected abstract IEnumerable<TServiceModel> GetAll();
    }
}
