﻿using System;
using System.Linq;
using System.Collections.Generic;

using PRM.Data.Models;
using PRM.Data.Repositories.Libraries;
using PRM.Service.Models.ServiceModels.Libraries;
using PRM.Service.Models.ServiceModels.Extensions;
using PRM.Service.Models.ServiceModels.Albums;

namespace PRM.Service.Managers
{
    public class LibraryManager : 
        Manager<Library, LibraryServiceModel, LibraryCreateServiceModel, 
            LibraryUpdateServiceModel, LibraryDeleteServiceModel>
    {
        /// <summary>
        /// Creates a new Library Manager
        /// </summary>
        /// <param name="repository">Injected repository</param>
        public LibraryManager(LibraryRepository repository) : base(repository)
        {
        }

        /// <summary>
        /// Creates a Library
        /// </summary>
        /// <param name="model">Create Service Model</param>
        /// <exception cref="NullReferenceException"></exception>
        /// <exception cref="DbEntityValidationException"></exception>
        public override LibraryServiceModel Create(LibraryCreateServiceModel model)
        {
            var library = model.ToLibrary();
            library = _repository.Create(library);

            return library.ToServiceModel();
        }

        /// <summary>
        /// Updates a Library
        /// </summary>
        /// <param name="model">Library Update Service Model</param>
        /// <exception cref="NullReferenceException"></exception>
        /// <exception cref="DbEntityValidationException"></exception>
        public override LibraryServiceModel Update(LibraryUpdateServiceModel model)
        {
            var library = _repository.Query.FirstOrDefault(l => l.Id == model.LibraryId);
            if (library == null) throw new InvalidOperationException("Library does not exist");

            library.Title = model.Title;
            library.Description = model.Description;

            var transaction = _repository.Context.SaveChanges();

            return library.ToServiceModel();
        }

        /// <summary>
        /// Deletes a Library
        /// </summary>
        /// <param name="model">Library Delete Service Model</param>
        /// <exception cref="NullReferenceException"></exception>
        /// <exception cref="DbEntityValidationException"></exception>
        public override LibraryServiceModel Delete(LibraryDeleteServiceModel model)
        {
            var library = _repository.Query.FirstOrDefault(f => f.Id == model.LibraryId);
            if (library == null) throw new InvalidOperationException("Library does not exist");

            library = _repository.Delete(library);

            return library.ToServiceModel();
        }

        /// <summary>
        /// Gets all Libraries as Service Models
        /// </summary>
        protected override IEnumerable<LibraryServiceModel> GetAll()
        {
            return _repository.All.Select(l => l.ToServiceModel());
        }

        /// <summary>
        /// Adds an existing Album to a Libary
        /// </summary>
        /// <param name="model"></param>
        /// <exception cref="InvalidOperationException"></exception>
        public LibraryServiceModel AddAlbum(AlbumAddServiceModel model)
        {
            var library = _repository.Query.FirstOrDefault(l => l.Id == model.LibraryId);
            if (library == null) throw new InvalidOperationException("Library does not exist");

            var album = _repository.Context.Albums.Find(model.AlbumId);
            if (album == null) throw new InvalidOperationException("Album does not exist");

            library.Albums.Add(album);
            _repository.Context.SaveChanges();

            return library.ToServiceModel();
        }

        /// <summary>
        /// Removes an existing Album from a Library
        /// </summary>
        /// <param name="model"></param>
        /// <exception cref="InvalidOperationException"></exception>
        public LibraryServiceModel RemoveAlbum(AlbumRemoveServiceModel model)
        {
            var library = _repository.Query.FirstOrDefault(l => l.Id == model.LibraryId);
            if (library == null) throw new InvalidOperationException("Library does not exist");

            var album = _repository.Context.Albums.Find(model.AlbumId);
            library.Albums.Remove(album);
            _repository.Context.SaveChanges();

            return library.ToServiceModel();
        }

        /// <summary>
        /// Moves an Album from one library to another
        /// </summary>
        /// <param name="model"></param>
        /// <exception cref="InvalidOperationException"></exception>
        public void MoveAlbum(AlbumMoveServiceModel model)
        {
            AddAlbum(new AlbumAddServiceModel()
            {
                LibraryId = model.LibraryTo,
                AlbumId = model.AlbumId
            });

            RemoveAlbum(new AlbumRemoveServiceModel()
            {
                LibraryId = model.LibraryFrom,
                AlbumId = model.AlbumId
            });
        }
    }
}
