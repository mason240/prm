﻿using System;
using System.Linq;

using PRM.Data.Models;
using PRM.Data.Repositories.Albums;

using PRM.Service.Models.ServiceModels.Albums;
using PRM.Service.Models.ServiceModels.Extensions;
using System.Collections.Generic;
using PRM.Service.Models.ServiceModels.Images;

namespace PRM.Service.Managers
{
    public class AlbumManager :
        Manager<Album, AlbumServiceModel, AlbumCreateServiceModel, AlbumUpdateServiceModel, AlbumDeleteServiceModel>
    {
        /// <summary>
        /// Creates a new Album Manager
        /// </summary>
        /// <param name="respository">Injected Repository</param>
        public AlbumManager(AlbumRepository respository) : base(respository)
        {
        }

        /// <summary>
        /// Creates an Album
        /// </summary>
        /// <param name="model"></param>
        public override AlbumServiceModel Create(AlbumCreateServiceModel model)
        {
            var album = model.ToAlbum();
            album = _repository.Create(album);

            return album.ToServiceModel();
        }

        /// <summary>
        /// Updates a Album
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override AlbumServiceModel Update(AlbumUpdateServiceModel model)
        {
            var album = model.ToAlbum();
            album = _repository.Update(album);

            return album.ToServiceModel();
        }

        /// <summary>
        /// Deletes an Album 
        /// </summary>
        /// <param name="model"></param>
        /// <exception cref="InvalidOperationException">/exception>
        public override AlbumServiceModel Delete(AlbumDeleteServiceModel model)
        {
            var album = _repository.Query.FirstOrDefault(a => a.Id == model.AlbumId);
            if (album == null) throw new InvalidOperationException("album does not exist");

            album = _repository.Delete(album);
            return album.ToServiceModel();
        }

        /// <summary>
        /// Gets All Albums 
        /// </summary>
        protected override IEnumerable<AlbumServiceModel> GetAll()
        {
            return _repository.Query.Select(a => a.ToServiceModel());
        }

        /// <summary>
        /// Adds an Image to an Album
        /// </summary>
        /// <param name="model"></param>
        /// <exception cref="InvalidOperationException"></exception>
        public AlbumServiceModel AddImage(ImageAddServiceModel model)
        {
            throw new NotImplementedException();

            //var album = _repository.Query.FirstOrDefault(a => a.Id == model.AlbumId);
            //if(album == null) throw new InvalidOperationException("Album does not exist");

            //var image = _repository.Context.Images.Find(model.ImageId);
            //if(image == null) throw new InvalidOperationException("Image does not exist");

            //album.AlbumImages .Images.Add(image);
            //_repository.Context.SaveChanges();

            //return album.ToServiceModel();
        }

        /// <summary>
        /// Removes an Image from an Album
        /// </summary>
        /// <param name="model"></param>
        /// <exception cref="InvalidOperationException"></exception>
        public AlbumServiceModel RemoveImage(ImageRemoveServiceModel model)
        {
            throw new NotImplementedException();

            //var album = _repository.Query.FirstOrDefault(a => a.Id == model.AlbumId);
            //if(album == null) throw new InvalidOperationException("Album does not exist");

            //var image = _repository.Context.Images.Find(model.ImageId);
            //if(image == null) throw new InvalidOperationException("Image does not exist");

            //album.Images.Remove(image);
            //_repository.Context.SaveChanges();

            //return album.ToServiceModel();
        }

        /// <summary>
        /// Moves an Image from a Source Album to a Source Album
        /// </summary>
        /// <param name="model"></param>
        /// <exception cref="InvalidOperationException"></exception>
        public AlbumServiceModel MoveImage(ImageMoveServiceModel model)
        {
            var service = AddImage(new ImageAddServiceModel()
            {
                AlbumId = model.AlbumDestination,
                ImageId = model.ImageId
            });

            service = RemoveImage(new ImageRemoveServiceModel()
            {
                AlbumId = model.AlbumSource,
                ImageId = model.ImageId
            });

            return service;
        }
    }
}
