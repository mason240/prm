﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PRM.Service.User;

namespace PRM.Service.User
{
    public interface IInviteService
    {
        bool SendInvite(string email);
        void SeedInvite(int count);
        void UseInvite(Guid inviteId, Guid libraryId);
        bool ValidateInvite(Guid inviteId);
    }
}
