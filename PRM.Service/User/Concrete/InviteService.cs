﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Threading.Tasks;
using PRM.Data.Database;

namespace PRM.Service.User.Concrete
{
    public class InviteService : IInviteService
    {
        // TO-DO
        //  Implement SendInvite() 

        private PrmContext _db;

        public InviteService(PrmContext context)
        {
            _db = context;
        }

        // Send an alpha invitation key. Returns false on db error.
        public bool SendInvite(string email)
        {
           // find an available key - doesn't handle unavailable keys
            var availableKey = _db.Invites.First(x => x.Email == null);
            
            // set this key's email attribute to the passed email, save changes
            try 
            {
                availableKey.Email = email;
                _db.SaveChanges();
            } 
            catch (Exception e)
            {
                // on error return false
                return false;
            }
            
            // build a link string to send to user, this is an example            
            string key = availableKey.Id.ToString("N");
            string linkString = ("http://www.prm.com/invite/" + key);
            // need to implement actual email delivery: address stored in 'email', send
            // the link stored in "linkString" - return false if any errors

            return true;
            

        }

        public void SeedInvite(int count)
        {
            throw new NotImplementedException();
        }

        public void UseInvite(Guid inviteId, Guid libraryId)
        {
            throw new NotImplementedException();
        }

        public bool ValidateInvite(Guid inviteId)
        {
            throw new NotImplementedException();
        }
    }
}
