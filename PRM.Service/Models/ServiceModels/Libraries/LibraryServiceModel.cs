﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PRM.Service.Models.ServiceModels.Libraries
{
    public class LibraryServiceModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
