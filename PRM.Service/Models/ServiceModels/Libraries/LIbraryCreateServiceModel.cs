﻿using System.ComponentModel.DataAnnotations;

namespace PRM.Service.Models.ServiceModels.Libraries
{
    public class LibraryCreateServiceModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
