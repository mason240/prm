﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PRM.Service.Models.ServiceModels.Libraries
{
    public class LibraryUpdateServiceModel
    {
        public Guid LibraryId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
