﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PRM.Service.Models.ServiceModels.Libraries
{
    public class LibraryDeleteServiceModel
    {
        public Guid LibraryId { get; set; }
    }
}
