﻿using System;

using PRM.Data.Models;
using PRM.Data.Models;
using PRM.Service.Models.ServiceModels.Albums;
using PRM.Service.Models.ServiceModels.Libraries;

namespace PRM.Service.Models.ServiceModels.Extensions
{
    public static class ServiceModelExtensions
    {
        #region libraries

        /// <summary>
        /// Converts from a LibraryCreateServiceModel to a Library model
        /// </summary>
        /// <param name="this"></param>
        public static Library ToLibrary(this LibraryCreateServiceModel @this)
        {
            var library = new Library()
            {
                Title = @this.Title,
                Description = @this.Description
            };

            return library;
        }

        /// <summary>
        /// Converts froma  LibraryDeleteServiceModel to a Library entity
        /// </summary>
        /// <param name="this"></param>
        public static Library ToLibrary(this LibraryDeleteServiceModel @this)
        {
            var library = new Library()
            {
                Id = @this.LibraryId
            };

            return library;
        }

        /// <summary>
        /// Converts a library to a Library Service Model
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static LibraryServiceModel ToServiceModel(this Library @this)
        {
            var service = new LibraryServiceModel()
            {
               Title = @this.Title,
               Description = @this.Description,
               CreatedOn = @this.TimeCreated
            };

            return service;
        }

        /// <summary>
        /// Converts to a LibraryCreateServiveModel
        /// </summary>
        /// <param name="library">Library</param>
        public static LibraryCreateServiceModel ToCreateServiceModel(this Library @this)
        {
            var service = new LibraryCreateServiceModel()
            {
                Description = @this.Description,
                Title = @this.Title
            };

            return service;
        }

        /// <summary>
        /// Converts from Library to LibraryDeleteServiceModel
        /// </summary>
        /// <param name="this"></param>
        public static LibraryDeleteServiceModel ToDeleteServiceModel(this Library @this)
        {
            var service = new LibraryDeleteServiceModel()
            {
                LibraryId = @this.Id
            };

            return service;
        }

        #endregion

        #region Albums

        /// <summary>
        /// Converts an Album to an Album Service Model
        /// </summary>
        /// <param name="this"></param>
        /// <exception cref="NullReferenceException"></exception>
        public static AlbumServiceModel ToServiceModel(this Album @this)
        {
            var service = new AlbumServiceModel()
            {
                Title = @this.Title,
                Description = @this.Description,
                CreatedOn = @this.TimeCreated
            };

            return service;
        }

        /// <summary>
        /// Converts an Album Create Service Model to a Album
        /// </summary>
        /// <param name="this"></param>
        /// <exception cref="NullReferenceExceptoin"></exception>
        public static Album ToAlbum(this AlbumCreateServiceModel @this)
        {
            var album = new Album()
            {
                Title = @this.Title,
                Description = @this.Description,
                TimeCreated = @this.CreatedOn
            };

            return album;
        }

        /// <summary>
        /// Converts a Album Update Service Model to a Album
        /// </summary>
        /// <param name="this"></param>
        /// <exception cref="NullReferenceException"></exception>
        public static Album ToAlbum(this AlbumUpdateServiceModel @this)
        {
            var album = new Album()
            {
                Id = @this.AlbumId,
                Title = @this.Title,
                Description = @this.Description
            };

            return album;
        }

        /// <summary>
        /// Converts an Album Delete Service Model to a Album
        /// </summary>
        /// <param name="this"></param>
        /// <exception cref="NullReferenceException"></exception>
        public static Album ToAlbum(this AlbumDeleteServiceModel @this)
        {
            var service = new Album()
            {
                Id = @this.AlbumId
            };

            return service;
        }

        /// <summary>
        /// Converts an Album Service Model to an Album 
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static Album ToAlbum(this AlbumServiceModel @this)
        {
            var album = new Album()
            {
                Id = @this.AlbumId,
                TimeCreated = @this.CreatedOn,
                Title = @this.Title,
                Description = @this.Description
            };

            return album;
        }

        #endregion

    }
}
