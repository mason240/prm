﻿using System.ComponentModel.DataAnnotations;
namespace PRM.Service.Models.ServiceModels.Users
{
    public class UserCreateServiceModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }
}
