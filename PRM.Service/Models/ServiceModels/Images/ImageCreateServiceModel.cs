﻿using System;

namespace PRM.Service.Models.ServiceModels.Images
{
    /// <summary>
    /// Service Model for Creating and Image
    /// </summary>
    public class ImageCreateServiceModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
