﻿using System;

namespace PRM.Service.Models.ServiceModels.Images
{
    /// <summary>
    /// Service Model to Remove and Image from
    /// an Album
    /// </summary>
    public class ImageRemoveServiceModel
    {
        public Guid AlbumId { get; set; }
        public Guid ImageId { get; set; }
    }
}
