﻿using System;

namespace PRM.Service.Models.ServiceModels.Images
{
    /// <summary>
    /// Service model for moving an image
    /// </summary> 
    public class ImageMoveServiceModel
    {
        /// <summary>
        /// Album source moving the Image from
        /// </summary>
        public Guid AlbumSource { get; set; }

        /// <summary>
        /// Album Destination moving the Image to
        /// </summary>
        public Guid AlbumDestination { get; set; }

        /// <summary>
        /// Image Id being moved
        /// </summary>
        public Guid ImageId { get; set; }
    }
}
