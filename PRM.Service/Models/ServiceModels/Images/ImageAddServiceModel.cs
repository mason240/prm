﻿using System;

namespace PRM.Service.Models.ServiceModels.Images
{
    /// <summary>
    /// Adds an Image to an Album
    /// </summary>
    public class ImageAddServiceModel
    {
        public Guid AlbumId { get; set; }
        public Guid ImageId { get; set; }
    }
}
