﻿using System;

namespace PRM.Service.Models.ServiceModels.Images
{
    /// <summary>
    /// Service Model for deleting and Image
    /// </summary>
    public class ImageDeleteServiceModel
    {
        public Guid ImageId { get; set; }
    }
}
