﻿using System;

namespace PRM.Service.Models.ServiceModels.Images
{
    /// <summary>
    /// Service Model for Updating an Image
    /// </summary>
    public class ImageUpdateServiceModel
    {
        public Guid ImageId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
