﻿using System;

namespace PRM.Service.Models.ServiceModels.Images
{
    /// <summary>
    /// Image Service Model
    /// </summary>
    public class ImageServiceModel
    {
        public Guid ImageId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
