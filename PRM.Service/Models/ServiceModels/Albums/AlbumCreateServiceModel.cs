﻿using System;
namespace PRM.Service.Models.ServiceModels.Albums
{
    public class AlbumCreateServiceModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
