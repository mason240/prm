﻿using System;

namespace PRM.Service.Models.ServiceModels.Albums
{
    /// <summary>
    /// Model for adding a Album to s Library
    /// </summary>
    public class AlbumAddServiceModel
    {
        public Guid LibraryId { get; set; }
        public Guid AlbumId { get; set; }
    }
}
