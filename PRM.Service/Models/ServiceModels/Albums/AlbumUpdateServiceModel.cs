﻿using System;
namespace PRM.Service.Models.ServiceModels.Albums
{
    public class AlbumUpdateServiceModel
    {
        public Guid AlbumId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
