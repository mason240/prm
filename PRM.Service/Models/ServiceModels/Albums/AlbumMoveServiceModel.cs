﻿using System;

namespace PRM.Service.Models.ServiceModels.Albums
{
    public class AlbumMoveServiceModel
    {
        public Guid LibraryTo { get; set; }
        public Guid LibraryFrom { get; set; }
        public Guid AlbumId { get; set; }
    }
}
