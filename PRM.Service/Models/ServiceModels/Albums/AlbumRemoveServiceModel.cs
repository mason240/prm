﻿using System;

namespace PRM.Service.Models.ServiceModels.Albums
{
    /// <summary>
    /// Service Model for removing an Album from a Library
    /// </summary>
    public class AlbumRemoveServiceModel
    {
        public Guid LibraryId { get; set; }
        public Guid AlbumId { get; set; }
    }
}
