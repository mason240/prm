﻿using System;
namespace PRM.Service.Models.ServiceModels.Albums
{
    /// <summary>
    /// Service Model for for Albums
    /// </summary>
    public class AlbumServiceModel
    {
        public Guid AlbumId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
