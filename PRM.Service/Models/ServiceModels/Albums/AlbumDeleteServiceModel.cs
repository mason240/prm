﻿using System;

namespace PRM.Service.Models.ServiceModels.Albums
{
    public class AlbumDeleteServiceModel
    {
        public Guid AlbumId { get; set; }
    }
}
