﻿using System;
using System.IO;
using System.Web;

namespace PRM.Service.Image
{
    public interface IImageStorageService
    {
        string GetImageUrl(Guid imageId);
        bool SaveImage(ImageStreamWrapper imageStreamWrapper);
        bool SaveImage(Guid imageId, HttpPostedFileBase imageStream);
        bool SaveImage(Guid imageId, MemoryStream imageStream, string contentType); // use this one
        MemoryStream GetImageStream(Guid imageId);
        ImageStreamWrapper GetImageStreamWrapper(Guid imageId);
        bool DeleteImage(Guid imageId);
    }

    public class ImageStreamWrapper
    {
        public MemoryStream ImageStream { get; set; }
        public Guid ImageId { get; set; }
        public string ContentType { get; set; }

        public ImageStreamWrapper()
        {

        }
    }
}
