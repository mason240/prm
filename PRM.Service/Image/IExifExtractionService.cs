﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace PRM.Service.Image
{
    public interface IExifExtractionService
    {
        PRM.Data.Models.Exif CollectExifData(Stream imageStream);
    }
}