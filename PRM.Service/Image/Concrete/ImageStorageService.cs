﻿using System;
using System.IO;
using System.Web;
using System.Web.ModelBinding;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using PRM.Service.Image;

namespace PRM.Service.Image.Concrete
{
    public class ImageStorageService : IImageStorageService
    {
        //TODO: Store ACCOUNT_NAME, ACCOUNT_KEY in web.config
        private const string BASE_IMG_URL = "https://prmimagestorage.blob.core.windows.net/prmimagestorage/";
        private const string CONTAINTER_NAME = "prmimagestorage";
        //private const string ACCOUNT_KEY = "NMXU3e62J7kzjMCM20irFhPCWVExjeZI2EXC1WGk7weQ9ZLwB8S72U9GE56hGwzxkB4TzklbAiIjOee8ekXtxQ==";

        private CloudStorageAccount storageAccount;
        private CloudBlobClient blobClient;
        private CloudBlobContainer container;

        public ImageStorageService()
        {
            storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("prmImageStorage"));
            blobClient = storageAccount.CreateCloudBlobClient();
            container = blobClient.GetContainerReference(CONTAINTER_NAME);

            //var permissions = container.GetPermissions();
            //permissions.PublicAccess = BlobContainerPublicAccessType.Container;
            //container.SetPermissions(permissions);

        }

        public bool GetImage(Guid imageId)
        {
            return false;
        }

        public string GetImageUrl(Guid imageId)
        {
            return BASE_IMG_URL + imageId.ToString("N");
        }

        public bool SaveImage(ImageStreamWrapper imageStreamWrapper)
        {
            return SaveImage(imageStreamWrapper.ImageId, imageStreamWrapper.ImageStream, imageStreamWrapper.ContentType);
        }

        public bool SaveImage(Guid imageId, HttpPostedFileBase imageStream)
        {
            try
            {
                string blobName = imageId.ToString("N");

                CloudBlockBlob blob = container.GetBlockBlobReference(blobName);
                blob.Properties.ContentType = imageStream.ContentType;
                blob.UploadFromStream(imageStream.InputStream);
                //var uriBuilder = new UriBuilder(blob.Uri);
                //uriBuilder.Scheme = "http";

                return true;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                return false;
            }
        }

        public bool SaveImage(Guid imageId, MemoryStream imageStream, string contentType) // use this one
        {
            try
            {
                string blobName = imageId.ToString("N");
                imageStream.Position = 0;

                CloudBlockBlob blob = container.GetBlockBlobReference(blobName);
                blob.Properties.ContentType = contentType;
                blob.UploadFromStream(imageStream);
                return true;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                return false;
            }
        }

        public MemoryStream GetImageStream(Guid imageId)
        {
            string blobName = imageId.ToString("N");

            CloudBlockBlob blob = container.GetBlockBlobReference(blobName);
            MemoryStream stream = new MemoryStream();
            blob.DownloadToStream(stream);
            return stream;
        }

        public ImageStreamWrapper GetImageStreamWrapper(Guid imageId)
        {
            string blobName = imageId.ToString("N");

            CloudBlockBlob blob = container.GetBlockBlobReference(blobName);
            MemoryStream stream = new MemoryStream();
            blob.DownloadToStream(stream);

            var imageStreamWrapper = new ImageStreamWrapper()
            {
                ImageId = imageId,
                ImageStream = stream,
                ContentType = blob.Properties.ContentType,
            };

            return imageStreamWrapper;
        }

        public bool DeleteImage(Guid imageId)
        {
            string blobName = imageId.ToString("N");

            CloudBlockBlob blob = container.GetBlockBlobReference(blobName);
            blob.DeleteIfExists();

            return true;
        }
    }


}
