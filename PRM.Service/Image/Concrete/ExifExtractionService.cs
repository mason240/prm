﻿using System;
using System.IO;
using PRM.Data.Models;

using ExifLib;

namespace PRM.Service.Image.Concrete
{
    public class ExifExtractionService : IExifExtractionService
    {
        public Exif CollectExifData(Stream imageStream)
        {
            Exif exif;

            try
            {
               
                exif = new Exif();

                Stream newStream = imageStream;


                using (ExifReader reader = new ExifReader(newStream))
                {
                    double DigitalZoomRatio, xRes, yRes, ShutterSpeedValue, exposureTime, FlashEnergy;
                    double[] GPSLat, GPSLong;
                    byte[] ExifVersion, FlashpixVersion, UserComment;
                    ushort YCbCrPositioning, Contrast, CustomRendered, ExposureProgram, Orientation, LightSource, Flash, ResolutionUnit, ColorSpace;
                    DateTime dateTime;
                    object description, GPSLatRef, GPSLongRef, Artist, BrightnessValue, Make, Model,
                        software, width, Height, Compression, DeviceSettingDescription, FNumber, Aperture,
                        ExposureBiasValue, ExposureIndex, ExposureMode, FocalLength,
                        FocalPlaneResolutionUnit, FocalPlaneXResolution, YCbCrCoefficients, YCbCrSubSampling,
                        WhitePoint, SubjectArea, SubjectDistance, SubjectDistanceRange, SubjectLocation;

                    if (reader.GetTagValue<double>(ExifTags.FlashEnergy, out FlashEnergy))
                    {
                        exif.FlashEnergy = FlashEnergy;
                        
                    }
                    if (reader.GetTagValue<object>(ExifTags.SubjectLocation, out SubjectLocation))
                    {
                        exif.SubjectLocation = SubjectLocation.ToString();
                    }
                    if (reader.GetTagValue<object>(ExifTags.SubjectDistanceRange, out SubjectDistanceRange))
                    {
                        exif.SubjectDistanceRange = SubjectDistanceRange.ToString();
                    }
                    if (reader.GetTagValue<object>(ExifTags.SubjectDistance, out SubjectDistance))
                    {
                        exif.SubjectDistance = SubjectDistance.ToString();
                    }
                    if (reader.GetTagValue<object>(ExifTags.SubjectArea, out SubjectArea))
                    {
                        exif.SubjectArea = SubjectArea.ToString();
                    }
                    if (reader.GetTagValue<object>(ExifTags.YCbCrSubSampling, out YCbCrSubSampling))
                    {
                        exif.YCbCrSubSampling = YCbCrSubSampling.ToString();
                    }
                    if (reader.GetTagValue<ushort>(ExifTags.YCbCrPositioning, out YCbCrPositioning))
                    {
                        switch (YCbCrPositioning)
                        {
                            case 1: exif.YCbCrPositioning = "Centered";
                                break;
                            case 2: exif.YCbCrPositioning = "Cosited";
                                break;
                            default: exif.YCbCrPositioning = "Value didn't fall within switch/case parameters";
                                break;
                        }

                    }
                    if (reader.GetTagValue<object>(ExifTags.WhitePoint, out WhitePoint))
                    {
                        exif.WhitePoint = WhitePoint.ToString();
                    }
                    if (reader.GetTagValue<object>(ExifTags.YCbCrCoefficients, out YCbCrCoefficients))
                    {
                        exif.YCbCrCoefficients = YCbCrCoefficients.ToString();
                    }
                    if (reader.GetTagValue<byte[]>(ExifTags.UserComment, out UserComment))
                    {

                        exif.UserComment = System.Text.Encoding.ASCII.GetString(UserComment);
                    }
                    if (reader.GetTagValue<byte[]>(ExifTags.FlashpixVersion, out FlashpixVersion))
                    {
                        exif.FlashpixVersion = System.Text.Encoding.ASCII.GetString(FlashpixVersion);
                    }
                    if (reader.GetTagValue<object>(ExifTags.FocalLength, out FocalLength))
                    {
                        exif.FocalLength = (double)FocalLength;
                    }
                    if (reader.GetTagValue<object>(ExifTags.FocalPlaneResolutionUnit, out FocalPlaneResolutionUnit))
                    {
                        exif.FocalPlaneResolutionUnit = FocalPlaneResolutionUnit.ToString();
                    }
                    if (reader.GetTagValue<object>(ExifTags.FocalPlaneXResolution, out FocalPlaneXResolution))
                    {
                        exif.FocalPlaneXResolution = FocalPlaneXResolution.ToString();
                    }

                    if (reader.GetTagValue<ushort>(ExifTags.ExposureProgram, out ExposureProgram))
                    {
                        switch (ExposureProgram)
                        {
                            case 0: exif.ExposureProgram = "Not defined";
                                break;
                            case 1: exif.ExposureProgram = "Manual";
                                break;
                            case 2: exif.ExposureProgram = "Normal program";
                                break;
                            case 3: exif.ExposureProgram = "Aperture priority";
                                break;
                            case 4: exif.ExposureProgram = "Shutter priority";
                                break;
                            case 5: exif.ExposureProgram = "Creative program (biased toward depth of field)";
                                break;
                            case 6: exif.ExposureProgram = "Action program (biased toward fast shutter speed)";
                                break;
                            case 7: exif.ExposureProgram = "Portrait mode (for closeup photos with the background out of focus)";
                                break;
                            case 8: exif.ExposureProgram = " Landscape mode (for landscape photos with the background in focus) ";
                                break;
                            default: exif.ExposureProgram = "Value didn't fall withing switch/case parameters";
                                break;
                        }
                        exif.ExposureProgram = ExposureProgram.ToString();
                    }
                    if (reader.GetTagValue<object>(ExifTags.ExposureMode, out ExposureMode))
                    {
                        exif.ExposureMode = (ushort)ExposureMode;
                    }
                    if (reader.GetTagValue<object>(ExifTags.ExposureIndex, out ExposureIndex))
                    {
                        exif.ExposureIndex = ExposureIndex.ToString();
                    }
                    if (reader.GetTagValue<object>(ExifTags.ExposureBiasValue, out ExposureBiasValue))
                    {
                        exif.ExposureBiasValue = (double)ExposureBiasValue;
                    }
                    if (reader.GetTagValue<ushort>(ExifTags.CustomRendered, out CustomRendered))
                    {
                        switch (CustomRendered)
                        {
                            case 0: exif.CustomRendered = "Normal Process";
                                break;
                            case 1: exif.CustomRendered = "Custom Process";
                                break;
                            default: exif.CustomRendered = "Value didn't fall withing switch/case parameters";
                                break;
                        }
                    }
                    if (reader.GetTagValue<ushort>(ExifTags.Contrast, out Contrast))
                    {
                        switch (Contrast)
                        {
                            case 0: exif.Contrast = "Normal";
                                break;
                            case 1: exif.Contrast = "Soft";
                                break;
                            case 2: exif.Contrast = "Hard";
                                break;
                            default: exif.Contrast = "Value didn't fall withing switch/case parameters";
                                break;
                        }

                    }
                    if (reader.GetTagValue<object>(ExifTags.BrightnessValue, out BrightnessValue))
                    {
                        exif.BrightnessValue = BrightnessValue.ToString();
                    }
                    if (reader.GetTagValue<double>(ExifTags.ExposureTime, out exposureTime))
                    {
                        exif.ExposureTime = exposureTime;
                    }
                    if (reader.GetTagValue<DateTime>(ExifTags.DateTimeDigitized, out dateTime))
                    {
                        exif.DateAndTime = dateTime;
                    }
                    if (reader.GetTagValue<byte[]>(ExifTags.ExifVersion, out ExifVersion))
                    {
                        exif.ExifVersion = System.Text.Encoding.ASCII.GetString(ExifVersion);
                    }
                    if (reader.GetTagValue<ushort>(ExifTags.Flash, out Flash))
                    {
                        exif.Flash = Flash;
                    }
                    if (reader.GetTagValue<object>(ExifTags.FNumber, out FNumber))
                    {
                        exif.FNumber = (double)FNumber;
                    }
                    if (reader.GetTagValue<object>(ExifTags.ApertureValue, out Aperture))
                    {
                        exif.Aperture = (double)Aperture;
                    }
                    if (reader.GetTagValue<object>(ExifTags.DeviceSettingDescription, out description))
                    {
                        exif.Description = description + "";
                    }
                    if (reader.GetTagValue<double[]>(ExifTags.GPSLatitude, out GPSLat))
                    {
                        //exif.GpsLatitude = GPSLat;
                        exif.GpsLatitudeDegrees = GPSLat[0];
                        exif.GpsLatitudeMinutes = GPSLat[1];
                        exif.GpsLatitudeSeconds = GPSLat[2];
                    }
                    if (reader.GetTagValue<object>(ExifTags.GPSLatitudeRef, out GPSLatRef))
                    {
                        exif.GpsLatitudeRef = GPSLatRef + "";
                    }
                    if (reader.GetTagValue<double[]>(ExifTags.GPSLongitude, out GPSLong))
                    {
                       // exif.GpsLongitude = GPSLong;
                        exif.GpsLongitudeDegrees = GPSLong[0];
                        exif.GpsLongitudeMinutes = GPSLong[1];
                        exif.GpsLongitudeSeconds = GPSLong[2];
                    }
                    if (reader.GetTagValue<object>(ExifTags.GPSLongitudeRef, out GPSLongRef))
                    {
                        exif.GpsLongitudeRef = GPSLongRef + "";
                    }
                    if (reader.GetTagValue<object>(ExifTags.Artist, out Artist))
                    {
                        exif.Artist = Artist + "";
                    }
                    if (reader.GetTagValue<ushort>(ExifTags.ColorSpace, out ColorSpace))
                    {
                        switch (ColorSpace)
                        {
                            case 0: exif.ColorSpace = "sRGB";
                                break;
                            case 65535: exif.ColorSpace = "Uncalibrated";
                                break;
                            default: exif.ColorSpace = "Value didn't fall withing switch/case parameters";
                                break;
                        }
                    }
                    if (reader.GetTagValue<double>(ExifTags.ShutterSpeedValue, out ShutterSpeedValue))
                    {
                        exif.ShutterSpeedValue = ShutterSpeedValue;
                    }
                    if (reader.GetTagValue<object>(ExifTags.Make, out Make))
                    {
                        exif.Make = Make + "";
                    }
                    if (reader.GetTagValue<object>(ExifTags.Model, out Model))
                    {
                        exif.Model = Model + "";
                    }
                    if (reader.GetTagValue<double>(ExifTags.XResolution, out xRes))
                    {
                        exif.XResolution = xRes;
                    }
                    if (reader.GetTagValue<double>(ExifTags.YResolution, out yRes))
                    {
                        exif.YResolution = yRes;
                    }
                    if (reader.GetTagValue<ushort>(ExifTags.LightSource, out LightSource))
                    {
                        switch (LightSource)
                        {
                            case 0: exif.LightSource = "Unknown";
                                break;
                            case 1: exif.LightSource = "Daylight";
                                break;
                            case 2: exif.LightSource = "Fluorescent";
                                break;
                            case 3: exif.LightSource = "Tungsten (incandescent light)";
                                break;
                            case 4: exif.LightSource = "Flash";
                                break;
                            case 9: exif.LightSource = "Fine weather";
                                break;
                            case 10: exif.LightSource = "Cloudy weather";
                                break;
                            case 11: exif.LightSource = "Shade";
                                break;
                            case 12: exif.LightSource = "Daylight fluorescent (D 5700 - 7100K)";
                                break;
                            case 13: exif.LightSource = "Day white fluorescent (N 4600 - 5400K)";
                                break;
                            case 14: exif.LightSource = "Cool white fluorescent (W 3900 - 4500K)";
                                break;
                            case 15: exif.LightSource = "White fluorescent (WW 3200 - 3700K)";
                                break;
                            case 17: exif.LightSource = "Standard Light A";
                                break;
                            case 18: exif.LightSource = "Standard Light B";
                                break;
                            case 19: exif.LightSource = "Standard Light C";
                                break;
                            case 20: exif.LightSource = "D55";
                                break;
                            case 21: exif.LightSource = "D65";
                                break;
                            case 22: exif.LightSource = "D75";
                                break;
                            case 23: exif.LightSource = "D50";
                                break;
                            case 24: exif.LightSource = "ISO studio tungsten";
                                break;
                            case 255: exif.LightSource = "Other light source";
                                break;
                            default: exif.LightSource = "Value didn't fall withing switch/case parameters";
                                break;
                        }

                    }
                    if (reader.GetTagValue<ushort>(ExifTags.ResolutionUnit, out ResolutionUnit))
                    {
                        switch (ResolutionUnit)
                        {
                            case 1: exif.ResolutionUnit = "No absolute unit of measurement.";
                                break;
                            case 2: exif.ResolutionUnit = "Inch";
                                break;
                            case 3: exif.ResolutionUnit = "Centimeter";
                                break;
                            default: exif.ResolutionUnit = "Value didn't fall withing switch/case parameters";
                                break;
                        }

                    }
                    if (reader.GetTagValue<object>(ExifTags.Software, out software))
                    {
                        exif.Software = software + "";
                    }
                    if (reader.GetTagValue<object>(ExifTags.ImageWidth, out width))
                    {
                        exif.Width = width + "";
                    }
                    if (reader.GetTagValue<object>(ExifTags.ImageLength, out Height))
                    {
                        exif.Height = Height + "";
                    }
                    if (reader.GetTagValue<ushort>(ExifTags.Orientation, out Orientation))
                    {
                        switch (Orientation)
                        {
                            case 1: exif.Orientation = "Top Left";
                                break;
                            case 2: exif.Orientation = "Top Right";
                                break;
                            case 3: exif.Orientation = "Bottom Right";
                                break;
                            case 4: exif.Orientation = "Bottom Left";
                                break;
                            case 5: exif.Orientation = "Left Top";
                                break;
                            case 6: exif.Orientation = "Right Top";
                                break;
                            case 7: exif.Orientation = "Right Bottom";
                                break;
                            case 8: exif.Orientation = "Left Bottom";
                                break;
                            default: exif.Orientation = "Value didn't fall withing switch/case parameters";
                                break;
                        }

                    }
                    if (reader.GetTagValue<object>(ExifTags.Compression, out Compression))
                    {
                        exif.Compression = Compression + "";
                    }
                    if (reader.GetTagValue<double>(ExifTags.DigitalZoomRatio, out DigitalZoomRatio))
                    {
                        exif.DigitalZoomRatio = DigitalZoomRatio;
                    }
                    if (reader.GetTagValue<object>(ExifTags.DeviceSettingDescription, out DeviceSettingDescription))
                    {
                        exif.DeviceSettingDescription = DeviceSettingDescription + "";
                    }

                }//end of using

                return exif;
                
            }//end of try
            catch (Exception e) 
            { 
                return null; 
            }

                       
            
        }

        
    }
}
