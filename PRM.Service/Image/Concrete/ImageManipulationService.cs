﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using ImageProcessor;
using PRM.Service.Image.Concrete;

namespace PRM.Service.Image.Concrete
{
    public class ImageManipulationService : IImageManipulationService
    {
        private ImageFactory imageFactory;
        private ImageStorageService imageStorageService;

        public ImageManipulationService()
        {
            //imageFactory = new ImageFactory(preserveExifData: true);
            imageFactory = new ImageFactory();
            imageStorageService = new ImageStorageService();
        }

        public bool RotateImage(Guid imageId, int rotationAngle)
        {
            if (rotationAngle < 0) rotationAngle = rotationAngle * -1;
            if ((rotationAngle > 360) || (rotationAngle%90 != 0)) return false;

            ImageStreamWrapper imageStreamWrapper = imageStorageService.GetImageStreamWrapper(imageId);
            MemoryStream outStream = new MemoryStream();
            imageFactory.Load(imageStreamWrapper.ImageStream)
                        .Rotate(rotationAngle)
                        .Save(outStream);

            imageStreamWrapper.ImageStream = outStream;
            imageStorageService.SaveImage(imageStreamWrapper);
            return true;
        }
    }
}
