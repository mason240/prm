﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRM.Service.Image
{
    public interface IImageManipulationService
    {
        bool RotateImage(Guid imageId, int rotationAngle);
    }
}
