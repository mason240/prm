﻿using System;
using System.Collections.Generic;
using PRM.Data.Models;

namespace PRM.Data.Repositories.Albums
{
    public interface IAlbumReposistory
    {
        /// <summary>
        /// Deletes an Album
        /// </summary>
        /// <param name="albumId">Album Id</param>
        Album Delete(Guid albumId);

        /// <summary>
        /// Creates an Album
        /// </summary>
        Album Create(Album album);

        /// <summary>
        /// Updates an Album
        /// </summary>
        Album Update(Album album);

        /// <summary>
        /// Finds all Albums for a User
        /// </summary>
        /// <param name="userId">Id of User</param>
        IEnumerable<Album> FindByUserId(Guid userId);
    }
}
