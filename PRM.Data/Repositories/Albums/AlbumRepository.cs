﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//using PRM.Data.Database;
//using PRM.Data.Models;

//namespace PRM.Data.Repositories.Albums
//{
//    public class AlbumRepository : Repository<Album>,  IAlbumReposistory
//    {
//        public AlbumRepository(IPrmContext context) 
//            : base(context)
//        {

//        }

//        public Album Delete(Guid albumId)
//        {
//            var album = _db.Albums.Find(albumId);
//            if (album == null) return null;

//            _db.Albums.Remove(album); // remvoe the album from the current context
//            _db.SaveChanges(); // commit transaction to db

//            return album;
//        }

//        public IEnumerable<Album> FindByUserId(Guid userId)
//        {
//            var user = _db.Users.Find(userId);

//            var userLibraries = _db.UserLibraries.Where(x => x.UserId == userId);
//            var albumsCollection = from albums in _db.Albums
//                         join uls in userLibraries on albums.LibraryId equals uls.Id
//                         select albums;

//            return user == null ? null : albumsCollection;
//        }
//    }
//}
