﻿using PRM.Data.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace PRM.Data.Repositories
{
    public class Repository<TEntity> where TEntity : class
    {
        protected readonly IPrmContext _db;

        /// <summary>
        /// Returns all Entities as Querable
        /// </summary>
        public IQueryable<TEntity> Query 
        {
            get 
            {
                return _db.Set<TEntity>();
            }
        }

        /// <summary>
        /// Returns all Entities as an Enumerable object
        /// </summary>
        public IEnumerable<TEntity> All
        {
            get
            {
                return _db.Set<TEntity>();
            }
        }

        /// <summary>
        /// Gets the Current Context
        /// </summary>
        public IPrmContext Context
        {
            get
            {
                return _db;
            }
        }

        /// <summary>
        /// Creates a new Base Repository
        /// </summary>
        /// <param name="context">Injected context</param>
        public Repository(IPrmContext context)
        {
            _db = context;
        }

        /// <summary>
        /// Creates an Entity in the datbase
        /// </summary>
        /// <param name="entity">entity to be created</param>
        public virtual TEntity Create(TEntity entity)
        {
            var tEntity = _db.Set<TEntity>().Add(entity);
            _db.SaveChanges(); // commit transaction to the database
            return tEntity;
        }

        /// <summary>
        /// Deletes an entity 
        /// </summary>
        /// <param name="entity">entity to delete</param>
        public virtual TEntity Delete(TEntity entity)
        {
            var delete = _db.Set<TEntity>().Remove(entity);
            _db.SaveChanges(); // commit transaction to the database
            return delete;
        }

        /// <summary>
        /// Updates an Entity
        /// </summary>
        /// <param name="entity">entity to update</param>
        /// <exception cref="NullReferenceException"></exception>
        public virtual TEntity Update(TEntity entity)
        {
            var keys = new List<object>();
            foreach(PropertyInfo info in entity.GetType().GetProperties())
            {
                foreach(var attribute in info.CustomAttributes)
                {
                    // if the property on the entity has the "Key" attribute
                    // add it the to the key collection
                    if (attribute.AttributeType.Equals(typeof(KeyAttribute))) keys.Add(info.GetValue(entity));
                }
            }

            var update = _db.Set<TEntity>().Find(keys);
            if(update == null) throw new NullReferenceException("Entity does not exist");

            foreach(PropertyInfo info in entity.GetType().GetProperties())
            {
                var propertyName = info.Name; // get the property name from the passed entity
                var propertyValue = info.GetValue(entity); // get the value from the passed entity

                // check if the property exists on the entity being updated
                var updateProperty = update.GetType().GetProperty(propertyName); 
                if(updateProperty == null) continue; // skip to the next property

                // Set the value on the Existing Entity to the Value on the entity being passed
                update.GetType().GetProperty(propertyName).SetValue(update, propertyValue);
            }

            _db.SaveChanges(); // save the updated property
            return update;
        }
    }
}
