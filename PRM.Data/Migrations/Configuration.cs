using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using PRM.Data.Database;
using PRM.Data.Models;
using System.Data.Entity.Migrations;

namespace PRM.Data.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<PrmContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            //AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(PrmContext context)
        {
            try
            {
                var seedData = new SeedData();

                var store = new UserStore<User>(context);
                var manager = new UserManager<User>(store);

                context.TimespanTypes.AddOrUpdate(x => x.Type,
                    seedData.timeSpanTypes.ToArray()
                );

                context.ContentTypes.AddOrUpdate(x => x.Type,
                        seedData.contentTypes.ToArray()
                );

                context.SaveChanges();

                foreach (var set in seedData.userSets)
                {
                    //new []{"Abel", "Adam", "Alley", "Ali"},
                    var adminUserEmail = set[1] + "@prm.com";

                    if (!context.Users.Any(u => u.UserName == adminUserEmail))
                    {
                        var users = new User[]
                        {
                            new User(){ FirstName = set[1], LastName = set[0], UserName = set[1] + "@prm.com", Email = set[1] + "@prm.com" },
                            new User(){ FirstName = set[2], LastName = set[0], UserName = set[2] + "@prm.com", Email = set[2] + "@prm.com" },
                            new User(){ FirstName = set[3], LastName = set[0], UserName = set[3] + "@prm.com", Email = set[3] + "@prm.com" },
                        };

                        manager.Create(users[0], seedData.defaultPassword);
                        manager.Create(users[1], seedData.defaultPassword);
                        manager.Create(users[2], seedData.defaultPassword);

                        var createdUsers = new List<User>();

                        foreach (var u in users)
                        {
                            createdUsers.Add(context.Users.First(x => x.FirstName == u.FirstName));
                        }

                        //Library
                        var library = new Library()
                        {
                            Title = set[0] + "'s Library",
                            Description = set[0] + "'s Library",
                            UserLibraries = new List<UserLibrary>()
                        };

                        //UserLibraries
                        foreach (var u in createdUsers)
                        {
                            library.UserLibraries.Add(new UserLibrary()
                            {
                                User = u,
                                Library = library
                            });
                        }

                        //Album
                        context.Albums.Add(new Album()
                        {
                            Title = "All 2013",
                            Description = "All 2013",
                            TimespanStart = new DateTime(2013, 1, 1),
                            TimespanEnd = new DateTime(2013, 12, 31),
                            TimespanType = seedData.timeSpanTypes.First(),
                            Library = library
                        });

                        context.Albums.Add(new Album()
                        {
                            Title = "All 2014",
                            Description = "All 2014",
                            TimespanStart = new DateTime(2014, 1, 1),
                            TimespanEnd = new DateTime(2014, 12, 31),
                            TimespanType = seedData.timeSpanTypes.First(),
                            Library = library
                        });
                        context.SaveChanges();
                    }
                }
            }
            catch (DbEntityValidationException ex)
            {
                var sb = new StringBuilder();

                foreach (var failure in ex.EntityValidationErrors)
                {
                    sb.AppendFormat("{0} failed validation\n", failure.Entry.Entity.GetType());
                    foreach (var error in failure.ValidationErrors)
                    {
                        sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                        sb.AppendLine();
                    }
                }

                throw new DbEntityValidationException(
                    "Entity Validation Failed - errors follow:\n" +
                    sb.ToString(), ex
                );
            }
            catch (Exception ex)
            {
                throw new Exception(
                    "Seed Failed - errors follow:\n" +
                    ex.Message, ex
                );
            }

        }
    }
}
