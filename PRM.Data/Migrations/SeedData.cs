﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using PRM.Data.Models;
using PRM.Data.Util;

namespace PRM.Data.Migrations
{
    public class SeedData
    {

        //Format for all emails will be {firstname}@prm.com
        //All passwords will be prm
        
        public string defaultPassword = "Password";
        public List<TimespanType> timeSpanTypes { get; set; }
        public List<ContentType> contentTypes { get; set; }
        public List<string[]> userSets { get; set; }

        public SeedData()
        {
            createData();
        }

        public void createData()
        {
            timeSpanTypes = new List<TimespanType>() { 
                 new TimespanType() { Id = 0, Type = "Placeholder" }
            };

            contentTypes = new List<ContentType>() {
                new ContentType() { Id = (int)ContentTypesEnum.LIBRARY, Type = ContentTypes.LIBRARY },
                new ContentType() { Id = (int)ContentTypesEnum.ALBUM, Type = ContentTypes.ALBUM },
                new ContentType() { Id = (int)ContentTypesEnum.SET, Type = ContentTypes.SET },
                new ContentType() { Id = (int)ContentTypesEnum.IMAGE, Type = ContentTypes.IMAGE }
            };

            //Last, First (library admin), First (user), First (user)
            userSets = new List<string[]>()
            {
                new []{"Abel", "Adam", "Alley", "Ali"},
                new []{"Baker", "Bell", "Ben", "Bill"},
                //new []{"Baker", "Bell", "Ben", "Bill"},
            };

            
        }
    }
}