namespace PRM.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemaneCommentcolumnParentCommentId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Comments", "ParentCommentId", c => c.Guid());
            DropColumn("dbo.Comments", "ParentContentId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Comments", "ParentContentId", c => c.Guid(nullable: false));
            DropColumn("dbo.Comments", "ParentCommentId");
        }
    }
}
