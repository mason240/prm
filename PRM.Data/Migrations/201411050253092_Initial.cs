namespace PRM.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AlbumImages",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        TimeCreated = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Album_Id = c.Guid(),
                        Image_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Albums", t => t.Album_Id)
                .ForeignKey("dbo.Images", t => t.Image_Id)
                .Index(t => t.Album_Id)
                .Index(t => t.Image_Id);
            
            CreateTable(
                "dbo.Albums",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Title = c.String(nullable: false, maxLength: 100),
                        Description = c.String(nullable: false, maxLength: 500),
                        HasTimespan = c.Boolean(nullable: false),
                        TimespanStart = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        TimespanEnd = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        TimeCreated = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Library_Id = c.Guid(),
                        CreatedByUser_Id = c.String(maxLength: 128),
                        TimespanType_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Libraries", t => t.Library_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedByUser_Id)
                .ForeignKey("dbo.TimespanTypes", t => t.TimespanType_Id)
                .Index(t => t.Library_Id)
                .Index(t => t.CreatedByUser_Id)
                .Index(t => t.TimespanType_Id);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        TimeCreated = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        FirstName = c.String(nullable: false, maxLength: 50),
                        LastName = c.String(nullable: false, maxLength: 50),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ParentContentId = c.Guid(nullable: false),
                        Text = c.String(nullable: false, maxLength: 500),
                        ContentId = c.Guid(nullable: false),
                        TimeCreated = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ContentType_Id = c.Int(),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ContentTypes", t => t.ContentType_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => t.ContentType_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.ContentTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Images",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Title = c.String(nullable: false, maxLength: 50),
                        Description = c.String(nullable: false, maxLength: 250),
                        DateTaken = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        TimeCreated = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ContentyType_Id = c.Int(),
                        UploadedByUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ContentTypes", t => t.ContentyType_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UploadedByUser_Id)
                .Index(t => t.ContentyType_Id)
                .Index(t => t.UploadedByUser_Id);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Persons",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        TimeCreated = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.UserLibraries",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        TimeCreated = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Library_Id = c.Guid(),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Libraries", t => t.Library_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => t.Library_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Libraries",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Title = c.String(maxLength: 50),
                        Description = c.String(maxLength: 250),
                        TimeCreated = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TimespanTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.String(nullable: false, maxLength: 25),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Exifs",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        FocalPlaneResolutionUnit = c.String(maxLength: 50),
                        FocalLength = c.Double(),
                        FlashpixVersion = c.String(maxLength: 50),
                        FocalPlaneXResolution = c.String(maxLength: 50),
                        ExposureProgram = c.String(maxLength: 50),
                        ExposureMode = c.Int(),
                        ExposureIndex = c.String(maxLength: 50),
                        ExposureBiasValue = c.Double(),
                        CustomRendered = c.String(maxLength: 50),
                        Contrast = c.String(maxLength: 50),
                        BrightnessValue = c.String(maxLength: 50),
                        Aperture = c.Double(),
                        DateAndTime = c.DateTime(),
                        Description = c.String(maxLength: 50),
                        ExposureTime = c.Double(),
                        FileName = c.String(maxLength: 50),
                        Flash = c.Double(),
                        GpsLatitudeRef = c.String(maxLength: 10),
                        GpsLongitudeRef = c.String(maxLength: 10),
                        Artist = c.String(maxLength: 50),
                        Copyright = c.String(maxLength: 50),
                        FNumber = c.Double(),
                        Height = c.String(maxLength: 50),
                        ColorSpace = c.String(maxLength: 50),
                        IsValid = c.String(maxLength: 50),
                        LightSource = c.String(maxLength: 50),
                        Make = c.String(maxLength: 50),
                        Model = c.String(maxLength: 50),
                        Orientation = c.String(maxLength: 50),
                        ResolutionUnit = c.String(maxLength: 50),
                        Software = c.String(maxLength: 50),
                        ThumbnailData = c.String(maxLength: 50),
                        ThumbnailOffset = c.String(maxLength: 50),
                        ThumbnailSize = c.String(maxLength: 50),
                        UserComment = c.String(maxLength: 50),
                        Width = c.String(maxLength: 50),
                        XResolution = c.Double(),
                        YResolution = c.Double(),
                        ShutterSpeedValue = c.Double(),
                        Compression = c.String(maxLength: 50),
                        DigitalZoomRatio = c.Double(),
                        DeviceSettingDescription = c.String(maxLength: 50),
                        ExifVersion = c.String(maxLength: 50),
                        YCbCrCoefficients = c.String(maxLength: 50),
                        WhitePoint = c.String(maxLength: 50),
                        YCbCrPositioning = c.String(maxLength: 50),
                        YCbCrSubSampling = c.String(maxLength: 50),
                        SubjectArea = c.String(maxLength: 50),
                        SubjectDistance = c.String(maxLength: 50),
                        SubjectDistanceRange = c.String(maxLength: 50),
                        SubjectLocation = c.String(maxLength: 50),
                        FlashEnergy = c.Double(),
                        GpsLatitudeDegrees = c.Double(),
                        GpsLatitudeMinutes = c.Double(),
                        GpsLatitudeSeconds = c.Double(),
                        GpsLongitudeDegrees = c.Double(),
                        GpsLongitudeMinutes = c.Double(),
                        GpsLongitudeSeconds = c.Double(),
                        TimeCreated = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Image_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Images", t => t.Image_Id)
                .Index(t => t.Image_Id);
            
            CreateTable(
                "dbo.Invites",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Email = c.String(maxLength: 100),
                        AffiliareEmail = c.String(maxLength: 100),
                        UserId = c.Guid(),
                        LibraryId = c.Guid(),
                        AffiliateUserId = c.Guid(),
                        TimeCreated = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Exifs", "Image_Id", "dbo.Images");
            DropForeignKey("dbo.Albums", "TimespanType_Id", "dbo.TimespanTypes");
            DropForeignKey("dbo.Albums", "CreatedByUser_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.UserLibraries", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.UserLibraries", "Library_Id", "dbo.Libraries");
            DropForeignKey("dbo.Albums", "Library_Id", "dbo.Libraries");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Persons", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Comments", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Images", "UploadedByUser_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Images", "ContentyType_Id", "dbo.ContentTypes");
            DropForeignKey("dbo.AlbumImages", "Image_Id", "dbo.Images");
            DropForeignKey("dbo.Comments", "ContentType_Id", "dbo.ContentTypes");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AlbumImages", "Album_Id", "dbo.Albums");
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.Exifs", new[] { "Image_Id" });
            DropIndex("dbo.UserLibraries", new[] { "User_Id" });
            DropIndex("dbo.UserLibraries", new[] { "Library_Id" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.Persons", new[] { "User_Id" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.Images", new[] { "UploadedByUser_Id" });
            DropIndex("dbo.Images", new[] { "ContentyType_Id" });
            DropIndex("dbo.Comments", new[] { "User_Id" });
            DropIndex("dbo.Comments", new[] { "ContentType_Id" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.Albums", new[] { "TimespanType_Id" });
            DropIndex("dbo.Albums", new[] { "CreatedByUser_Id" });
            DropIndex("dbo.Albums", new[] { "Library_Id" });
            DropIndex("dbo.AlbumImages", new[] { "Image_Id" });
            DropIndex("dbo.AlbumImages", new[] { "Album_Id" });
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Invites");
            DropTable("dbo.Exifs");
            DropTable("dbo.TimespanTypes");
            DropTable("dbo.Libraries");
            DropTable("dbo.UserLibraries");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.Persons");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.Images");
            DropTable("dbo.ContentTypes");
            DropTable("dbo.Comments");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Albums");
            DropTable("dbo.AlbumImages");
        }
    }
}
