﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PRM.Data.Models;

namespace PRM.Data.Util
{
    public enum ContentTypesEnum
    {
        LIBRARY = 0,
        ALBUM = 1,
        SET = 2,
        IMAGE = 3
    }

    public class ContentTypes
    {
        public const string LIBRARY = "Library";
        public const string ALBUM = "Album";
        public const string SET = "Set";
        public const string IMAGE = "Image";
    }
}
