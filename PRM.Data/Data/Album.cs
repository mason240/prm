
namespace PRM.Data.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Album
    {
        public Album()
        {
            this.AlbumImages = new HashSet<AlbumImage>();
            this.Albums1 = new HashSet<Album>();
            this.Albums11 = new HashSet<Album>();
        }
    
        public System.Guid Id { get; set; }
        public System.DateTime TimeCreated { get; set; }
        public System.Guid CreatedByUserId { get; set; }
        public System.Guid LibraryId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool HasTimespan { get; set; }
        public System.DateTime TimespanStart { get; set; }
        public System.DateTime TimespanEnd { get; set; }
        public int TimespanType { get; set; }
        public System.Guid BranchRootId { get; set; }
        public System.Guid BranchParentId { get; set; }
    
        public virtual ICollection<AlbumImage> AlbumImages { get; set; }
        public virtual Library Library { get; set; }
        public virtual ICollection<Album> Albums1 { get; set; }
        public virtual Album Album1 { get; set; }
        public virtual ICollection<Album> Albums11 { get; set; }
        public virtual Album Album2 { get; set; }
    }
}
