namespace PRM.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Image : BaseModel
    {
        public Image()
        {
            AlbumImages = new HashSet<AlbumImage>();
        }

        [Required]
        [StringLength(50)]
        public string Title { get; set; }

        [Required]
        [StringLength(250)]
        public string Description { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime DateTaken { get; set; }

        //public Guid BranchRootId { get; set; }
        //public Guid BranchParentId { get; set; }

        public virtual User UploadedByUser { get; set; }

        public virtual ContentType ContentyType { get; set; }

        public virtual ICollection<AlbumImage> AlbumImages { get; set; }

        //public virtual Exif Exif { get; set; }
    }
}
