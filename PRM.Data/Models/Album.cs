namespace PRM.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Album : BaseModel
    {
        public Album()
        {
            AlbumImages = new HashSet<AlbumImage>();
            //var id = Guid.NewGuid();
            //Id = id;
            //BranchParentAlbumId = id;
            //BranchRootAlbumId = id;
        }

        [Required]
        [StringLength(100)]
        public string Title { get; set; }

        [Required]
        [StringLength(500)]
        public string Description { get; set; }

        public bool HasTimespan { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime TimespanStart { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime TimespanEnd { get; set; }

        //public int TimespanTypeId { get; set; }
        public virtual TimespanType TimespanType { get; set; }

        //[Required]
        //public Guid BranchRootAlbumId { get; set; }
        //public virtual Album BranchRootAlbum { get; set; }

        //[Required]
        //public Guid BranchParentAlbumId { get; set; }
        //public virtual Album BranchParentAlbum { get; set; }

        public virtual User CreatedByUser { get; set; }

        public virtual Library Library { get; set; }

        public virtual ICollection<AlbumImage> AlbumImages { get; set; }

    }
}
