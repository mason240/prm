namespace PRM.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Text;
    using System.Threading.Tasks;

    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;


    /// <summary>
    /// This inheirits from IdentityUser.  That means that it already has all the things that come with the IdentityUser.
    /// Roles, Email, etc. etc.  This is mapped to the Database by the MvtlContext to the AspNetUser table bceause of the
    /// MvtlContext extending IdentityDbContext<AppUser>
    /// 
    /// To use inside the context use Users (not AppUsers)
    /// 
    /// Questions ask RDROST or MTHORNBERG
    /// </summary>
    [Table("Users")]
    public class User : IdentityUser
    {
        public User()
        {
            TimeCreated = DateTime.Now;

            Comments = new HashSet<Comment>();
            Images = new HashSet<Image>();
            Persons = new HashSet<Person>();
            UserLibraries = new HashSet<UserLibrary>();
        }

        //public string Id
        //{
        //    get { return base.Id; }
        //}

        [Column(TypeName = "datetime2")]
        public DateTime TimeCreated { get; set; }

        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50)]
        public string LastName { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }

        public virtual ICollection<Image> Images { get; set; }

        public virtual ICollection<Person> Persons { get; set; }

        public virtual ICollection<UserLibrary> UserLibraries { get; set; }


        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);

            // Add custom user claims here
            return userIdentity;
        }

        public Task<ClaimsIdentity> GenerateUserIdentity(UserManager<User> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);

            // Add custom user claims here
            return userIdentity;
        }
    }
}
