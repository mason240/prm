namespace PRM.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class UserLibrary : BaseModel
    {
        //public Guid Id { get; set; }

        //[Column(TypeName = "datetime2")]
        //public DateTime TimeCreated { get; set; }

        //public Guid UserId { get; set; }

        //public Guid LibraryId { get; set; }

        //public Guid LibraryId { get; set; }

        //public Guid UserId { get; set; }

        public virtual Library Library { get; set; }

        public virtual User User { get; set; }
    }
}
