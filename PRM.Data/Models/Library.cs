namespace PRM.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Library : BaseModel
    {
        public Library()
        {
            Albums = new HashSet<Album>();
            UserLibraries = new HashSet<UserLibrary>();
        }

        //public Guid Id { get; set; }

        //[Column(TypeName = "datetime2")]
        //public DateTime TimeCreated { get; set; }

        [StringLength(50)]
        public string Title { get; set; }

        [StringLength(250)]
        public string Description { get; set; }

        public virtual ICollection<Album> Albums { get; set; }

        public virtual ICollection<UserLibrary> UserLibraries { get; set; }
    }
}
