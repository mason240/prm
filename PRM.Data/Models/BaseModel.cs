﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRM.Data.Models
{
    public class BaseModel
    {
        public BaseModel()
        {
            Id = Guid.NewGuid();
            TimeCreated = DateTime.Now;
        }

        [Key]
        public Guid Id { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime TimeCreated { get; set; }
    }
}
