namespace PRM.Data.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public partial class Comment : BaseModel 
    {
        [Required]
        [StringLength(500)]
        public string Text { get; set; }

        public Guid ContentId { get; set; }

        public Guid? ParentCommentId { get; set; }

        public virtual ContentType ContentType { get; set; }

        public virtual User User { get; set; }
    }
}
