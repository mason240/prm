namespace PRM.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Invite : BaseModel
    {
        //public Guid Id { get; set; }

        public Invite()
        {

        }

        [StringLength(100)]
        public string Email { get; set; }

        [StringLength(100)]
        public string AffiliareEmail { get; set; }

        public Guid? UserId { get; set; }

        public Guid? LibraryId { get; set; }

        public Guid? AffiliateUserId { get; set; }
        
    }
}
