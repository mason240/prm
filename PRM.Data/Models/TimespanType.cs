namespace PRM.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TimespanType
    {
        public int Id { get; set; }

        [Required]
        [StringLength(25)]
        public string Type { get; set; }
    }
}
