namespace PRM.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class AlbumImage : BaseModel
    {
        //public Guid Id { get; set; }

        //[Column(TypeName = "datetime2")]
        //public DateTime TimeCreated { get; set; }

        //public Guid AlbumId { get; set; }

        //public Guid ImageId { get; set; }

        public virtual Album Album { get; set; }

        public virtual Image Image { get; set; }
    }
}
