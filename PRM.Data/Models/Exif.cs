namespace PRM.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Exif : BaseModel
    {

        [StringLength(50)]
        public string FocalPlaneResolutionUnit { get; set; }

        public double? FocalLength { get; set; }

        [StringLength(50)]
        public string FlashpixVersion { get; set; }

        [StringLength(50)]
        public string FocalPlaneXResolution { get; set; }

        [StringLength(50)]
        public string ExposureProgram { get; set; }

        public int? ExposureMode { get; set; }

        [StringLength(50)]
        public string ExposureIndex { get; set; }

        public double? ExposureBiasValue { get; set; }

        [StringLength(50)]
        public string CustomRendered { get; set; }

        [StringLength(50)]
        public string Contrast { get; set; }

        [StringLength(50)]
        public string BrightnessValue { get; set; }

        public double? Aperture { get; set; }

        public DateTime? DateAndTime { get; set; }

        [StringLength(50)]
        public string Description { get; set; }

        public double? ExposureTime { get; set; }

        [StringLength(50)]
        public string FileName { get; set; }

        public double? Flash { get; set; }

        [StringLength(10)]
        public string GpsLatitudeRef { get; set; }

        [StringLength(10)]
        public string GpsLongitudeRef { get; set; }

        [StringLength(50)]
        public string Artist { get; set; }

        [StringLength(50)]
        public string Copyright { get; set; }

        public double? FNumber { get; set; }

        [StringLength(50)]
        public string Height { get; set; }

        [StringLength(50)]
        public string ColorSpace { get; set; }

        [StringLength(50)]
        public string IsValid { get; set; }

        [StringLength(50)]
        public string LightSource { get; set; }

        [StringLength(50)]
        public string Make { get; set; }

        [StringLength(50)]
        public string Model { get; set; }

        [StringLength(50)]
        public string Orientation { get; set; }

        [StringLength(50)]
        public string ResolutionUnit { get; set; }

        [StringLength(50)]
        public string Software { get; set; }

        [StringLength(50)]
        public string ThumbnailData { get; set; }

        [StringLength(50)]
        public string ThumbnailOffset { get; set; }

        [StringLength(50)]
        public string ThumbnailSize { get; set; }

        [StringLength(50)]
        public string UserComment { get; set; }

        [StringLength(50)]
        public string Width { get; set; }

        public double? XResolution { get; set; }

        public double? YResolution { get; set; }

        public double? ShutterSpeedValue { get; set; }

        [StringLength(50)]
        public string Compression { get; set; }

        public double? DigitalZoomRatio { get; set; }

        [StringLength(50)]
        public string DeviceSettingDescription { get; set; }

        [StringLength(50)]
        public string ExifVersion { get; set; }

        [StringLength(50)]
        public string YCbCrCoefficients { get; set; }

        [StringLength(50)]
        public string WhitePoint { get; set; }

        [StringLength(50)]
        public string YCbCrPositioning { get; set; }

        [StringLength(50)]
        public string YCbCrSubSampling { get; set; }

        [StringLength(50)]
        public string SubjectArea { get; set; }

        [StringLength(50)]
        public string SubjectDistance { get; set; }

        [StringLength(50)]
        public string SubjectDistanceRange { get; set; }

        [StringLength(50)]
        public string SubjectLocation { get; set; }

        public double? FlashEnergy { get; set; }

        public double? GpsLatitudeDegrees { get; set; }

        public double? GpsLatitudeMinutes { get; set; }

        public double? GpsLatitudeSeconds { get; set; }

        public double? GpsLongitudeDegrees { get; set; }

        public double? GpsLongitudeMinutes { get; set; }

        public double? GpsLongitudeSeconds { get; set; }

        //public int ImageId { get; set; }

        public virtual Image Image { get; set; }
    }
}
