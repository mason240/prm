﻿using PRM.Data.Database;
using PRM.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRM.Data.Repository
{
    public interface ICommentRepository
    {
        IEnumerable<Comment> GetCommentsForContent(Guid contentId);
        Comment CreateComment(Comment comment);
    }
}
