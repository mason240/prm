﻿using PRM.Data.Models;
using System;
using System.Collections.Generic;

namespace PRM.Data.Repository
{
    public interface IAlbumRepository
    {
        IEnumerable<Library> GetLibrariesForUser(Guid userId);
        IEnumerable<Album> GetAlbumsForUser(Guid userId);
        Album GetAlbumById(Guid Id);
        bool DeleteAlbum(Guid albumId);
        Album CreateAlbum(Album album);
        IEnumerable<Image> GetImagesForAlbum(Guid albumId);
        AlbumImage CreatAlbumImage(AlbumImage albumImage);
        AlbumImage CreatAlbumImage(Guid albumId, Guid imageId);
        IEnumerable<AlbumImage> GetAlbumImagesForAlbum(Guid imageId, Guid albumId);
        IEnumerable<Library> GetLibrariesWithAlbumsForUser(Guid userId);
    }

}
