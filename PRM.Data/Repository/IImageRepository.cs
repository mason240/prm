﻿using PRM.Data.Database;
using PRM.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRM.Data.Repository
{
    public interface IImageRepository
    {
        Image CreateImage(Image image);
        bool DeleteImageFromAlbum(Guid imageId, Guid albumId);
    }
}
