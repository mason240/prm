﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PRM.Data.Database;
using PRM.Data.Models;

namespace PRM.Data.Repository.Concretes
{
    public class AlbumRepository : IAlbumRepository
    {
        private PrmContext _db;

        public AlbumRepository(PrmContext prmContext)
        {
            _db = prmContext;
        }

        public Album GetAlbumById(Guid Id)
        {
            return _db.Albums.Find(Id);
        }

        public IEnumerable<Library> GetLibrariesForUser(Guid userId)
        {
            var stringUserId = userId.ToString();

            var libraries = from userLibrary in _db.UserLibraries
                            join library in _db.Libraries on userLibrary.Library.Id equals library.Id
                            where userLibrary.User.Id == stringUserId
                            select library;

            return libraries;
        }

        public IEnumerable<Album> GetAlbumsForUser(Guid userId)
        {
            //TODO: Mason verify this works

            var libraries = GetLibrariesForUser(userId);

            //TODO: FK Fix
            var albums = from album in _db.Albums
                         where album.Library.Id == album.Id
                         select album;

            //var albums = from library in libraries
            //             join album in _db.Albums on library.Id equals album.LibraryId
            //             select album;

            return albums;
        }

        public IEnumerable<Image> GetImagesForAlbum(Guid albumId)
        {
            var images = from image in _db.Images
                         join albumImage in _db.AlbumImages on image.Id equals albumImage.Image.Id
                         where albumImage.Album.Id == albumId
                         select image;

            return images;
        }

        public Album CreateAlbum(Album album)
        {
            //Root/Partent BranchIds are set in model class


            album.CreatedByUser = _db.Users.Find(album.CreatedByUser.Id);
            album.TimespanType = _db.TimespanTypes.Find(album.TimespanType.Id);
            album.Library = _db.Libraries.Find(album.Library.Id);

            _db.Albums.Add(album);
            _db.SaveChanges();

            return album;
        }

        public bool DeleteAlbum(Guid albumId)
        {
            var album = GetAlbumById(albumId);
            if (album == null) return false;

            _db.Albums.Remove(album);
            _db.SaveChanges();

            return true;
        }

        public AlbumImage CreatAlbumImage(AlbumImage albumImage)
        {
            var image = _db.Images.Find(albumImage.Image.Id);
            var album = _db.Albums.Find(albumImage.Album.Id);

            albumImage.Image = image;
            albumImage.Album = album;

            _db.AlbumImages.Add(albumImage);
            _db.SaveChanges();
            return albumImage;
        }

        public AlbumImage CreatAlbumImage(Guid albumId, Guid imageId)
        {
            //TODO: FK Fix
            var album = new Album() { Id = albumId };
            var image = new Image() { Id = imageId };
            var albumImage = new AlbumImage()
            {
                Album = album,
                Image = image
            };

            return CreatAlbumImage(albumImage);
        }

        public IEnumerable<AlbumImage> GetAlbumImagesForAlbum(Guid imageId, Guid albumId)
        {
            return _db.AlbumImages.Where(x => x.Image.Id == imageId).ToList();
        }

        public IEnumerable<Library> GetLibrariesWithAlbumsForUser(Guid userId)
        {
            //TODO: Mason fix this

            var stringUserId = userId.ToString();

            var userLibraries = _db.UserLibraries.Where(x => x.User.Id == stringUserId);

            var libraries = from lib in _db.Libraries
                            join usLib in userLibraries on lib.Id equals usLib.Library.Id
                            select lib;

            return libraries;


            //TODO: Adust eager/lazy loading
            //IEnumerable<Library> blah = _db.Libraries
            //    .Join(_db.Users,
            //    library => library.Id,
            //    userLibrary => userLibrary.Id,
            //    (library, userLibrary) => new { library = library, userLibrary = userLibrary })
            //    .Where(x => x.userLibrary.Id == stringUserId)
            //    .Include(x => x.library.Albums)
            //    .Select(x => x.library);

        }
    }

}
