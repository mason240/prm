﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PRM.Data.Database;
using PRM.Data.Models;

namespace PRM.Data.Repository.Concretes
{
    public class ImageRepository : IImageRepository
    {
        private PrmContext _db;

        public ImageRepository(PrmContext prmContext)
        {
            _db = prmContext;
        }

        public Image CreateImage(Image image)
        {
            if (string.IsNullOrWhiteSpace(image.Title)) image.Title = "d";
            if (string.IsNullOrWhiteSpace(image.Description)) image.Description = "d";

            var user = _db.Users.Find(image.UploadedByUser.Id);
            var contentType = _db.ContentTypes.Find(image.ContentyType.Id);

            image.UploadedByUser = user;
            image.ContentyType = contentType;

            _db.Images.Add(image);
            _db.SaveChanges();

            return image;
        }

        public bool DeleteImageFromAlbum(Guid imageId, Guid albumId)
        {
            //TODO FK Fix
            var albumImageList = _db.AlbumImages.Where(x => x.Image.Id == imageId).ToList();

            //if image is only in one album, delete image
            if (albumImageList.Count > 2)
            {
                var image = _db.Images.Find(imageId);
                _db.Images.Remove(image);
            }

            var albumImage = albumImageList.FirstOrDefault(x => x.Album.Id == albumId);
            _db.AlbumImages.Remove(albumImage);
            _db.SaveChanges();

            return true;
        }

    }

}
