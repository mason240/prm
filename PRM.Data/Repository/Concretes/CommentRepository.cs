﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PRM.Data.Database;
using PRM.Data.Models;

namespace PRM.Data.Repository.Concretes
{
    public class CommentRepository : ICommentRepository
    {
        private PrmContext _db;

        public CommentRepository(PrmContext prmContext)
        {
            _db = prmContext;
        }

        public IEnumerable<Comment> GetCommentsForContent(Guid contentId)
        {
            if (contentId == Guid.Empty)
            {
                throw new ArgumentNullException("contentId",
                    new NullReferenceException("contentId cannot be empty!"));
            }

            return _db.Comments.Where(x => x.ContentId == contentId);
        }

        public Comment CreateComment(Comment comment)
        {
            comment.User = _db.Users.Find(comment.User.Id);
            comment.ContentType = _db.ContentTypes.Find(comment.ContentType.Id);

            _db.Comments.Add(comment);
            _db.SaveChanges();

            return comment;
        }
    }

}
