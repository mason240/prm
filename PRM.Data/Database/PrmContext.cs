using Microsoft.AspNet.Identity.EntityFramework;
using PRM.Data.Models;

namespace PRM.Data.Database
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// Context for the database.
    /// 
    /// Extending IdentityDbContext<AppUser> gives the line to us:
    ///   public virtual IDbSet<AppUser> Users { get; set; }
    /// </summary>
    //public partial class PrmContext : IdentityDbContext<User>, IPrmContext
    public class PrmContext : IdentityDbContext<User>
    {
        public PrmContext()
            : base("PrmContext")
        {
        }

        public virtual DbSet<AlbumImage> AlbumImages { get; set; }
        public virtual DbSet<Album> Albums { get; set; }
        public virtual DbSet<Comment> Comments { get; set; }
        public virtual DbSet<ContentType> ContentTypes { get; set; }
        public virtual DbSet<Exif> Exifs { get; set; }
        public virtual DbSet<Image> Images { get; set; }
        public virtual DbSet<Invite> Invites { get; set; }
        public virtual DbSet<Library> Libraries { get; set; }
        public virtual DbSet<Person> Persons { get; set; }
        public virtual DbSet<TimespanType> TimespanTypes { get; set; }
        public virtual DbSet<UserLibrary> UserLibraries { get; set; }
        //public virtual DbSet<User> Users { get; set; }
    }
}