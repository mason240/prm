﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using PRM.Data.Models;

namespace PRM.Data.Database
{
    public interface IPrmContext : IDbContext
    {
        DbSet<Image> Images { get; set; }
        DbSet<Album> Albums { get; set; }
        DbSet<AlbumImage> AlbumImages { get; set; }
        DbSet<User> Users { get; set; }
        DbSet<Exif> Exifs { get; set; }
        DbSet<Comment> Comments { get; set; }
        DbSet<ContentType> ContentTypes { get; set; }
        DbSet<Library> Libraries { get; set; }
        DbSet<Person> Persons { get; set; }
        DbSet<TimespanType> TimespanTypes { get; set; }
        DbSet<UserLibrary> UserLibraries { get; set; }
    }
}
