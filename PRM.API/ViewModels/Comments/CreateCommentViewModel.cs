﻿using PRM.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRM.API.ViewModels.Comments
{
    public class CreateCommentViewModel
    {
        public Comment Comment { get; set; }
    }
}