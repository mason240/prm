﻿using PRM.Data.Models;
using System.Web.Mvc;

namespace PRM.API.ViewModels.Album
{
    public class CreateAlbumViewModel
    {
        public PRM.Data.Models.Album Album { get; set; }
        public SelectList LibraryDropdown { get; set; }
    }
}