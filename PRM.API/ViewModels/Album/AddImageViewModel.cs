﻿using PRM.Data.Models;
using System;
using System.Web;

namespace PRM.API.ViewModels.Album
{
    public class AddImageViewModel
    {
        public Guid AlbumId { get; set; }
        public Image Image { get; set; }
        public HttpPostedFileBase ImageFile { get; set; }
        public Image[] Images { get; set; }
    }
}