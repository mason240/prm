﻿using PRM.Data.Models;
using System.Collections.Generic;

namespace PRM.API.ViewModels.Album
{
    public class AlbumIndexViewModel
    {
        public PRM.Data.Models.Album album { get; set; }

        public IList<Image> images { get; set; }
        public IList<Comment> comments { get; set; }
    }
}