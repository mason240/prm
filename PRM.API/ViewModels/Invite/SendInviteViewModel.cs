﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace PRM.API.ViewModels.Invite
{
    public class SendInviteViewModel
    {
        [Required]
        public string Email { get; set; }
    }
}