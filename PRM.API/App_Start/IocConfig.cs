﻿using System.Reflection;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using PRM.API.Controllers;
using PRM.API.Controllers.Concrete;
using PRM.API.Controllers.Concretes;
using PRM.Service.User;
using PRM.Service.User.Concrete;
using PRM.Service.Image;
using PRM.Service.Image.Concrete;
using PRM.Data.Database;

namespace PRM.API.App_Start
{
    public class IocConfig
    {
        public static void RegisterDependencies()
        {
            var builder = new ContainerBuilder();
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.RegisterType<PrmContext>()
                .As<PrmContext>()
                .InstancePerRequest();

            //PRM.API ApiControllers
            builder.RegisterType<AlbumApiController>()
                .As<IAlbumApiController>()
                .InstancePerRequest();

            builder.RegisterType<CommentApiController>()
                .As<ICommentApiController>()
                .InstancePerRequest();

            builder.RegisterType<ImageApiController>()
                .As<IImageApiController>()
                .InstancePerRequest();

            builder.RegisterType<LibraryApiController>()
                .As<ILibraryApiController>()
                .InstancePerRequest();

            builder.RegisterType<InviteApiController>()
                .As<IInviteApiController>()
                .InstancePerRequest();

            //PRM.Service Services
            builder.RegisterType<ImageStorageService>()
                .As<IImageStorageService>()
                .InstancePerRequest();

            builder.RegisterType<ImageManipulationService>()
                .As<IImageManipulationService>()
                .InstancePerRequest();

            builder.RegisterType<ExifExtractionService>()
                .As<IExifExtractionService>()
                .InstancePerRequest();

            IContainer container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}