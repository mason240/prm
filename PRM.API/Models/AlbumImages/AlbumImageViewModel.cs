﻿using System;

namespace PRM.API.Models.AlbumImages
{
    public class AlbumImageViewModel
    {
        public Guid ImageId { get; set; }
        public Guid AlbumId { get; set; }
    }
}