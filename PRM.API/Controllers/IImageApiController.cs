﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PRM.API.ViewModels.Album;

namespace PRM.API.Controllers
{
    public interface IImageApiController
    {
        AddImageViewModel GetAddImageViewModel(Guid albumId);
        bool AddImageToAlbum(AddImageViewModel viewModel);
        bool RotateImage(Guid albumId, Guid imageId, int rotationAngle);
        bool DeleteImageFromAlbum(Guid userId, Guid imageId, Guid albumId);
    }
}
