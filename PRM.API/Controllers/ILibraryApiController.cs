﻿using PRM.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRM.API.Controllers
{
    public interface ILibraryApiController
    {
        IEnumerable<Library> GetLibrariesWithAlbumsForUser(Guid userId);

    }
}