﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.UI.WebControls;
using PRM.API.ViewModels.Comments;
using PRM.Data.Database;
using PRM.Data.Models;
using PRM.Data.Repository;
using PRM.Data.Repository.Concretes;
using PRM.Data.Util;

namespace PRM.API.Controllers
{
    public class CommentApiController : BaseApiController, ICommentApiController
    {
        private PrmContext _db;
        private ICommentRepository _commentRepository;

        public CommentApiController(PrmContext db)
        {
            _db = db;
            _commentRepository = new CommentRepository(_db);
        }

        public CreateCommentViewModel GetCreateCommentViewModel(Guid userId, Guid contentId, int contentTypeId)
        {
            ContentType contentType = new ContentType()
            {
                Id = contentTypeId
                //Type = ContentTypes.ALBUM
            };

            var comment = new Comment()
            {
                ContentId = contentId,
                ParentCommentId = contentId,
                ContentType = contentType
            };

            var viewModel = new CreateCommentViewModel()
            {
                Comment = comment
            };

            return viewModel;
        }

        public Comment CreateComment(CreateCommentViewModel viewModel)
        {

            var comment = _commentRepository.CreateComment(viewModel.Comment);

            return comment;
        }
    }
}