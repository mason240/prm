﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using PRM.API.Controllers.Concretes;
using PRM.API.ViewModels.Album;
using PRM.Data.Repository.Concretes;
using PRM.Service.Image;
using PRM.Service.Image.Concrete;
using PRM.Data.Database;
using PRM.Data.Models;
using PRM.Data.Repository;

namespace PRM.API.Controllers
{
    public class AlbumApiController : BaseApiController, IAlbumApiController
    {
        private PrmContext _db;
        private IImageStorageService _imageStorageService;
        private IAlbumRepository _albumRepository;
        private ICommentRepository _commentRepository;

        //public AlbumApiController(IPrmContext db, IImageStorageService imageStorageService)
        public AlbumApiController(PrmContext context)
        {
            _db = context;
            _imageStorageService = new ImageStorageService();
            _albumRepository = new AlbumRepository(_db);
            _commentRepository = new CommentRepository(_db);
        }

        public AlbumIndexViewModel GetAlbumIndexViewModel(Guid albumId)
        {
            var album = _albumRepository.GetAlbumById(albumId);
            var images = _albumRepository.GetImagesForAlbum(albumId).ToList();
            var comments = _commentRepository.GetCommentsForContent(albumId).ToList();

            var model = new AlbumIndexViewModel()
            {
                album = album,
                images = images,
                comments = comments
            };

            return model;
        }

        public CreateAlbumViewModel GetCreateAlbumViewModel(Guid userId)
        {
            var album = new Album();
            var userLibraries = _albumRepository.GetLibrariesForUser(userId).ToList();
            var libraryDropdown = createSelectList(userLibraries);
            var model = new CreateAlbumViewModel()
            {
                Album = album,
                LibraryDropdown = libraryDropdown
            };
            
            return model;
        }

        public Album CreateAlbum(CreateAlbumViewModel viewModel)
        {

            if (viewModel.Album.Id == Guid.Empty)
                new NullReferenceException("albumId cannot be null.");

            if (viewModel.Album.CreatedByUser.Id == String.Empty) throw new ArgumentNullException("userId",
                     new NullReferenceException("userId cannot be null."));

            //TODO: FK Fix
            //if (viewModel.Album.LibraryId == Guid.Empty) throw new ArgumentNullException("libraryId",
            //         new NullReferenceException("libraryId cannot be null."));

            var album = _albumRepository.CreateAlbum(viewModel.Album);

            return viewModel.Album;
        }

        public bool DeleteAlbum(Guid albumId)
        {
            return _albumRepository.DeleteAlbum(albumId);
        }

        public IEnumerable<Library> GetLibrariesWithAlbumsForUser(Guid userId)
        {
            return _albumRepository.GetLibrariesForUser(userId);
        }

        //TODO: Move to something better
        private SelectList createSelectList(List<Library> libraries)
        {
            var listItems = new List<SelectListItem>();

            foreach (var x in libraries)
            {
                listItems.Add(new SelectListItem()
                {
                    Value = x.Id.ToString("N"),
                    Text = x.Title
                });
            }
            return new SelectList(listItems, "Value", "Text");
        }

    }
}
