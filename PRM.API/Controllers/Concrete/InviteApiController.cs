﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PRM.API.ViewModels.Invite;
using PRM.Service.User;

namespace PRM.API.Controllers.Concrete
{
    public class InviteApiController : BaseApiController, IInviteApiController
    {
        // TO-DO
        //   
        
        private IInviteService _inviteService;
        
        public InviteApiController(IInviteService inviteService) 
        {
            _inviteService = inviteService;
        }

        public SendInviteViewModel GetSendInviteViewModel()
        {
            return new SendInviteViewModel();
        }

        public bool SendInvite(SendInviteViewModel viewModel)
        {
            // passes an email address to the service layer to send out an
            // invitation email, will return true if successful

            string email = viewModel.Email;
            return _inviteService.SendInvite(email);
        }
        public void Index(Guid inviteId)
        {
            //   - check if id is valid
            //   - check if id is primary or affiliate id
            //   - create user
            //   - create library
            //   - update Invite database table to associate key with UserId & LibraryId
            //   - create Affiliate user
            //   - send invite email to affiliate
            _inviteService.ValidateInvite(inviteId);
        }
    }
}