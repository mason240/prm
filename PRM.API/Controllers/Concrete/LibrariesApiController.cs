﻿using PRM.Data.Database;
using PRM.Data.Models;
using PRM.Data.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PRM.Data.Repository.Concretes;

namespace PRM.API.Controllers
{
    public class LibraryApiController : BaseApiController, ILibraryApiController
    {
        private PrmContext _db;
        private IAlbumRepository _albumRepository;
        public LibraryApiController(PrmContext db)
        {
            _db = db;
            _albumRepository = new AlbumRepository(_db);
        }

        public IEnumerable<Library> GetLibrariesWithAlbumsForUser(Guid userId)
        {
            var libraries = _albumRepository.GetLibrariesWithAlbumsForUser(userId);
            return libraries;
        }

    }
}

//TODO Dlete this line
