﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PRM.API.ViewModels.Album;
using PRM.Data.Repository.Concretes;
using PRM.Service.Image;
using PRM.Service.Image.Concrete;
using System.IO;
using PRM.Data.Database;
using PRM.Data.Models;
using PRM.API.Models.AlbumImages;
using PRM.Data.Repository;
using PRM.Data.Util;

namespace PRM.API.Controllers.Concretes
{
    public class ImageApiController : BaseApiController, IImageApiController
    {
        private PrmContext _db;
        private IImageStorageService _imageStorageService;
        private IImageManipulationService _imageManipulationService;
        private IExifExtractionService _exifExtractionService;
        private IAlbumRepository _albumRepository;
        private IImageRepository _imageRepository;

        //public ImageApiController(IPrmContext context, IImageStorageService imageStorageService)
        public ImageApiController(PrmContext context)
        {
            _db = context;
            _imageStorageService = new ImageStorageService();
            _imageManipulationService = new ImageManipulationService();
            _exifExtractionService = new ExifExtractionService();
            _albumRepository = new AlbumRepository(_db);
            _imageRepository = new ImageRepository(_db);
        }

        public AddImageViewModel GetAddImageViewModel(Guid albumId)
        {
            var viewModel = new AddImageViewModel()
            {
                Image = new Image()
                {
                    ContentyType = new ContentType() { Id = (int)ContentTypesEnum.IMAGE }
                },
                AlbumId = albumId,
            };

            return viewModel;
        }

        public bool AddImageToAlbum(AddImageViewModel viewModel)
        {
            var image = _imageRepository.CreateImage(viewModel.Image);
            var albumImage = _albumRepository.CreatAlbumImage(viewModel.AlbumId, image.Id);

            //TODO: Refactor this
            //var exifResult = SaveExifData(viewModel.ImageFile.InputStream, image.Id);

            var result = _imageStorageService.SaveImage(image.Id, viewModel.ImageFile);

            return result;
        }

        private object _exifExtractorService(System.IO.Stream stream)
        {
            throw new NotImplementedException();
        }

        public bool RotateImage(Guid albumId, Guid imageId, int rotationAngle)
        {
            if (rotationAngle < 0) rotationAngle = rotationAngle * -1;
            if ((rotationAngle > 360) || (rotationAngle % 90 != 0)) return false;

            var result = _imageManipulationService.RotateImage(imageId, rotationAngle);

            return result;
        }

        public bool DeleteImageFromAlbum(Guid userId, Guid imageId, Guid albumId)
        {
            var blobDeleteResult = _imageStorageService.DeleteImage(imageId);
            var deleteResult = _imageRepository.DeleteImageFromAlbum(imageId, albumId);

            return true;
        }

        public bool SaveExifData(Stream imageStream, Guid imageId)
        {
            //try
            //{
            //    Exif exif = _exifExtractionService.CollectExifData(imageStream);

            //    exif.ImageId = imageId;

            //    _db.Exifs.Add(exif);
            //    _db.SaveChanges();
            //    return true;
            //}
            //catch (Exception e)
            //{
            //    return false;
            //}
            return true;
        }

    }
}