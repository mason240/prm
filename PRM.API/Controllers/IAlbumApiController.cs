﻿using System;
using System.Collections.Generic;
using PRM.API.ViewModels.Album;
using PRM.Data.Models;

namespace PRM.API.Controllers.Concretes
{
    public interface IAlbumApiController
    {
        AlbumIndexViewModel GetAlbumIndexViewModel(Guid albumId);
        CreateAlbumViewModel GetCreateAlbumViewModel(Guid userId);
        Album CreateAlbum(CreateAlbumViewModel viewModel);
        bool DeleteAlbum(Guid albumId);
        IEnumerable<Library> GetLibrariesWithAlbumsForUser(Guid userId);
    }
}