﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using PRM.API.ViewModels.Invite;

namespace PRM.API.Controllers
{
    public interface IInviteApiController
    {
        SendInviteViewModel GetSendInviteViewModel();
        bool SendInvite(SendInviteViewModel viewModel);
        void Index(Guid inviteId);
    }
}
