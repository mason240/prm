﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using PRM.API.ViewModels.Comments;
using PRM.Data.Models;

namespace PRM.API.Controllers
{
    public interface ICommentApiController
    {

        CreateCommentViewModel GetCreateCommentViewModel(Guid userId, Guid contentId, int contentTypeId);
        Comment CreateComment(CreateCommentViewModel viewModel);
        //IQueryable<Comment> GetCommentsByContentId(string ContentId);
        //IQueryable<Comment> GetComments();

        #region default api controller methods

        //IHttpActionResult GetComment(Guid id);
        //IHttpActionResult PutComment(Guid id, Comment comment);
        //IHttpActionResult PostComment(Comment comment);
        //IHttpActionResult DeleteComment(Guid id);

        #endregion
    }
}